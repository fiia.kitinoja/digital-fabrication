+++
title = "About Page"
description = "This is the about page."
type = "about"
+++

### Hello!

My name is Fiia Kitinoja, and I am a Master's student in Aalto University, Helsinki, studying Landscape Architecture. I am participating in the Digital Fabrication 2022 course, run by Aalto University in collaboration with the Fab Academy, and I will use this page to document my progress throughout it. 
