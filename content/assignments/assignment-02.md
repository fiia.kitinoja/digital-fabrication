+++
title = "Week 2: Principles and Practices"
+++

## Learning more about Hugo

Quickly after setting up the first version of the website I realised that the template I had chosen was good for a blog, but since in this course we need to create multiple static pages, for example a page for the final project as well, I realised I needed to edit my site. Rather than editing the template, I decided to try and build the site myself using hugo. Kris advised me to look up the recorded lecture from previous year's Digital Fabrication course on hugo, which I did. In addition to watching the video I also referenced hugo's documentation, to get more information on specifics I didn't understand. The video I mentioned is embedded below. 

{{< youtube 7wCidM35tp8 >}}

I started by moving the existing directory structure to a hidden .temp folder so that I could start from a clean slate without losing any of my previous work. After that I created a new hugo website. 

``` bash
mkdir .temp
mv ./* .temp/
hugo new site . --force
```

After that I removed the unnecessary folders so that the directory structure is cleaner. After that I edited the config file with my website name and base URL. Then I created a index.html file in the layouts folder, and added some basic HTML, that included a header, navigation, main body of text, and a footer. Within the html I used hugo parameters that refer to the config file for the base URL and the site title, as instructed in the video. Example below.

``` bash
    <title>{{ .Site.Title }}</title>
``` 

Following further with the video I created a subfolder called partials inside the layouts folder, and created a nav.html file. I then moved the navigation HTML code within that file, and referenced it within the index.html.

``` bash
            {{ partial "nav.html" . }}
``` 

I did the same for the header and the footer, replacing the "nav.html" with the corresponding filenames. I copied over the documentation page from assignment 1, created new pages for about and final project sections, and made a subfolder in layouts called _default. Within that I created single.html file as per instructions in the video. This is pretty much where the video left off, but I had gotten into the groove and wanted to keep going. I wanted to learn more about how hugo work, and stumbled upon [this](https://www.jakewiesler.com/blog/hugo-directory-structure) blog post. It explained very well the main components of hugo, and gave me some more insight. 

At this point all my links were working, however they were missing some content. I had filled in the markdown file for my Week 01 assignment page, but the content wasn't displaying. I felt I was missing a connection between the layout file, and the content. After digging through a little bit of the hugo documentation, I found the line of code I needed to add. 

``` bash
        <main>
            {{ .Content }}
         </main>
``` 

I am not 100% sure if the main-tags need to be added, but I felt it wouldn't hurt. After adding the line the files connected and the content was now showing on the page. At this point I was happy and felt that I understood how the directory structure works, and I wanted to move on to customising my site. 

## Bootstrap!

TBC...