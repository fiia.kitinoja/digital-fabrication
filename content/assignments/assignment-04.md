+++
title = "Week 4 : Computer-aided Design"
+++

## Week 4: Computer-aided Design

After getting introduced to Computer-Aided Design tools in the global and local lectures, our assignment for this week is to use these CAD tools to design a possible final project. The assignment is broken down further into tasks:

* Explore one or more 2D vector graphics tools
* Explore one or more 2D raster graphics tools
* Explore one or more 3D modelling tools 
* Document your process with screenshots and descriptions
* Create page containing the assignment documentation on your website

On this page I will go through my experience exploring these different programs, and how they could help me in bringing my final project to reality.

## Prior experience with CAD

As a landscape architecture student I have experience working with CAD-programs in both university and work environments. The program that I have used and am the most familiar with is AutoCAD, however as it is a paid program I will be learning to use FreeCAD. Even though I at the moment have access to it freely as a student, I don't want to be too reliant on it in the future. 

I am also very familiar with some other vector and raster programs, mainly Adobe Photoshop and Illustrator. I dislike Adobe's subscription based plans, as they get very expensive in the long run, however they are the industry standard and so I have gotten used to using them. A cheaper alternative for Photoshop and Illustrator is [Affinity Photo and Designer](https://affinity.serif.com/en-us/), which are only a one-time payment of about 50 dollars each, so they are very affordable even for a hobbyist. I have experience with using Affinity Designer, which is their vector program, on my iPad and have enjoyed it a lot.

Despite having experience with these programs, the way that I have used them previously differs quite a bit from the way I will be using them for Digital fabrication. I have mainly used CAD-programs for 2D design, so learning 3D will be my main obstacle. I have some rudimentary knowledge of 3D drawing, but I'm excited to learn more. 

I also have prior experience using the laser cutter at fablab. I designed and lasercut Catan-boardgame out of plywood as a wedding gift for my sister and her husband, who got married in January. Below are some photos of the project.

![Catan tile designs](/catan1.jpg)

Illustrator file with the designs of the different types of board tiles

![Catan final Illustrator file](/catan2.png)

Final illustrator file with cut and etch lines. The size of the artboard is 900x600 mm, which was the size of my plywood sheet, and I was quite happy to see how I managed to fit everything on the same sheet.

![plywood after finishing laser cut](/catan3.jpg)

Photo of the plywood sheet after the laser cut finished

![Assembled and painted tiles](/catan4.jpg)

Game board after finishing assembly and painting.

## Utilising tools for my final project

At the moment I am working on making a bell siphon for the growbed, to regulate water flow from the grow bed to the fishtank. I have some 20 mm diameter PVC pipe that I want to use for the project, and I want to design and 3d print parts to help turn the pipes into a bell siphon. For the bell part I am thinking of using a pint glass from IKEA I have at home. 

In terms of the different tools I have broken down the process of designing the 3d parts in to the different categories of tools.

* 2D raster graphics tool: Sketching and noting necessary parameters for the different parts.
* 2D vector graphics tool: Drawing top and side profiles of the parts that are to scale to make sure the dimensions can work together and to help me make them into 3D
* 3D modeling tool: Creating the 3D models to 3D print

## Website Housekeeping

Before jumping in to the assignment of the week, I wanted to document a bit of website housekeeping. The size of the files included in the repository are important to keep small for both the size of the total repository, as well as the user experience for the visiters of the website. The smaller the file is, the faster it loads.

So far I have been resizing and compressing the images manually opening each image in photoshop and doing it there. This process gets tiring and time consuming, so I wanted to look into automating the process. I first created a folder where I would save all of the full size images.

``` bash
cd ..
mkdir dig-fab_images
```

Then I needed software to do the resizing and compressing of the image. After some research I saw [Imagemagick](https://imagemagick.org/index.php) to be a qood choice. I followed the instructions to install the software. 

``` bash
brew install ghostscript
brew install imagemagick
```

Then after doing some more research through multiple stackoverflow posts I ended up with the following command to grab the images from the source folder and saving them in the static folder of my website directory, resized and compressed. After they've been compressed I will remove the images from the folder and move them to another one to keep the original images. 

``` bash
cd dig-fab_images
mogrify -quality 95 -resize 600x -path /users/fiiakitinoja/projects/digital-fabrication/static/images -format jpg *.png
```


## 2D Raster graphics tools: Procreate

I wasn't sure how else I could use the raster graphics tools at this point for my final project, other than sketching my ideas. I have used raster graphics tools usually either for touching up images, building photo montages / visualisations for projects, or drawing. Drawing with a mouse is quite hard, so I decided to use my iPad and Apple pencil and draw the sketch that way. For drawing I used a raster graphics tool called Procreate. It is a fantastic program for the iPad and costs only about 10 or so euros. 

![iPad and Procreate](/assignment4-sketch3.jpg)

I drew a few sketches of the parts I need to build the bell siphon for my project, and added notes for the different parts and dimensions so that next steps would be easier to carry on. 

![Sketch 1](/assignment4-sketch1.jpg)

![Sketch 2](/assignment4-sketch2.jpg)

A nice feature of Procreate is that it automatically records a timelapse of the drawing process, and can easily be exported out. 

{{< video src="/assignment4-sketchvideo.webm" type="video/webm" preload="auto" >}}

## 2D Vector graphics tools: Illustrator

For the next step I ended up using Illustrator. I installed and tried out Inkscape, however I quickly found out I may be too set in my ways... I have gotten so used to the Adobe interface I found it difficult to work with inkscape. However I am planning on returning to Inkscape later on as I would like to learn to use that program as well. 

Nevertheless as I had sketched out the parts that I needed I wanted to draw vector images of the top and side views, that were to scale according to the dimensions I had set out. I created a new file and used mostly the shape tools that illustrator offers. For the first part top view I used only the ellipse tool, and defined the dimensions of the shape in millimeteres on the properties window. 

![Vector 1](/assignment4-vector1.jpg)

After that it was mostly just repeating the same actions to draw the rest of the shapes. For the side view I used the rectangle tool and similarly defined the dimensions. For the second part I again used the ellipse tool for top view, and rectangle tool for the side view. To get the diagonal shape of the top part correct, I drew two rectangles on top of each other, one 20x10mm and one 30x10mm. I used the direct selection tool to grab the two top nodes of the smaller rectangle, and moved them to where they snapped to the top two nodes of the bigger rectangle. After that I deleted the bigger rectangle and used the pathfinder tool to unite the neck rectangle and the siphon shape. 

![Vector 1](/assignment4-vector2.jpg)

## 3D Modeling tools: FreeCAD

#### Installation

I went to FreeCAD's [GitHub repository](https://github.com/FreeCAD/FreeCAD) and scrolled down to see the documentation. Following the instructions I went to the releases page and downloaded the correct file for my computer, which is running macOS.

![FreeCAD installation](/freecad1.jpg)

![FreeCAD installation](/freecad2.jpg)

After installing freecad it feels a bit cumbersome to open, as it doesn't seem to install to the laptop, so I will always have to search for the dmg file and open it from there. It might have some advantages that I am not aware of or maybe I installed it wrong, however I thought to note it here anyways. 

I'm watching the recorded [lecture](https://www.youtube.com/watch?v=V1P4bhulIaU) from last year and following along to create my part in Freecad. 

![FEM workspace](/images/freecad_FEM.jpg)

The FEM workspace mentioned in the beginning of the lecture is something I will look into, as I've been thinking of making a smaller scale build of my aquaponics tank first, as a proof of concept, and next week as our task is to cut something with the laser cutter I wanted to use the opportunity to cut the pieces of acrylic for the tank and the holding structure from plywood. Water can get very heavy, so I'll have to make sure the structure can hold the weight, for which this workspace can come in useful.

I created a new project for my part and start by defining some variables. I switch on the spreadsheet workspace and create a new spreadsheet.

![spreadsheet workspace](/images/freecad_spreadsheetWS.jpg)

![new spreadsheet](/images/freecad_newss.jpg)

After that I defined the variables that I had noted down during my sketching stage. I rethought some dimensions, and changed them for the variables. After typing all the dimensions in I right click on the dimension and go to Properties, and then Alias tab. I set the Alias for the cell as the description of the variable, using camel case as the alias cannot contain spaces. After that I go back to part design workspace and create a new body.

![variable dimensions](/images/freecad_dimensions.jpg)

![cell alias](/images/freecad_alias.jpg)

![New body](/images/freecad_newbody.jpg)

After that the first thing I will do is create a sketch.

![Create sketch](/images/freecad_newbody.jpg)

![XY Plane](/images/freecad_xyplane.jpg)

I chose to be working on the XY plane for the sketch. I use the circle tool to make a random size circle startin from the origin. 

![Circle tool](/images/freecad_drawcircle.jpg)

![Circle](/images/freecad_circle.jpg)

Then I select the circle and fix the diameter to a certain width using the tool shown below. 

![Fixed diameter](/images/freecad_diamConst.jpg)

![Formula editor](/images/freecad_formulaeditor.jpg)

In the dialog window textfield that appears I click the little formula editor icon to open another dialog box. In that textfield I connect the circle diameter with the InnerGlassDiameter variable of the spreadsheet I created earlier. 

![Constrained circle](/images/freecad_constrainedcircle.jpg)

Then by clicking ok the circle's diameter now corresponds to the InnerGlassDiameter variable.

I repeat the action specified above to create two more circles within the bigger circle, and constraint the diameters to the PipeOuterDiameter. I move the left circle to touch the origin point as that will be where the riser pipe will come in. At the top of the riser pipe will be attached the 3D printed funnel, so that will need more space. The other circle I moved further to the right.

![Two circles](/images/freecad_twocircles.jpg)

The final element for this top view sketch is to draw the outer circle, that shows the lip width. I repeat the same steps as before, and as a variable use GlassInnerDiameter + LipWidth.

![outer lip](/images/freecad_outerlip.jpg)

After that we can close the sketch and use the pad sketch tool. 

![pad sketch](/images/freecad_padsketch.jpg)

At this point I realised I may have made a mistake in my sketching phase, as I wasn't sure which element of the sketch I should select to pad. I went back into the sketch to delete the inner circles, and then padded the outer lip, using the MiddleLayerThickness variable.

![finished sketch v1](/images/freecad_finishedsketch1.jpg)

![outer lip](/images/freecad_paddedOuterLip.jpg)

I then selected the top face of the body, and created a new sketch to draw on it. I made a new circle with the variable GlassInnerDiameter and closed the sketch. Then I did the same for the bottom face.

![Top sketch](/images/freecad_topfacesketch.jpg)

![outer lip](/images/freecad_innercircle.jpg)

Then I selected each new circle separately and padded them in the same way I did previously.

![Padded shape](/images/freecad_paddedlayers.jpg)

I then selected the top face again and redrew the smaller circles using the PipeOuterDiameter variable.

![Small circles sketch](/images/freecad_sketchSmallCircles.jpg)

![Padded shape with small circles](/images/freecad_paddedsmallcircles.jpg)

Then for this part the only thing left to do is to to create the holes using the sketched lines. I select the create hole tool after selecting the circles, specify diameter using the PipeOutlineDiameter variable and the depth by using TopLayerThickness + MiddleLayerThickness + Bottom Layer Thickness.

![Holes1](/images/freecad_holes1.jpg)

![Holes2](/images/freecad_holes2.jpg)

After this the base part of my bell siphon is ready, and I export it as an .stl file for 3d printing. I opened the stl in Cura, and sliced it with the low quality settings, and sent it to my 3d printer. 

![Cura slicer](/images/cura_bellsiphonbase.jpg)

While the base is printing, I decided to draw create the funnel. Thinkin over how to create it, I thought the easiest way to draw would be to show a cut section of one side, and then padd it by revolving 360 degrees. So I created a new body, and a new sketch on the XZ axis.

![new sketch](/images/freecad_funnelsketch.jpg)

I started by draw a line, and then making it vertical by using the vertical constraint tool. Then I constrained it to the FunnelNeckHeight variable. 

![Funnel neck](/images/freecad_funnelneck.jpg)

![Funnel neck variable](/images/freecad_funnelneckvariable.jpg)

I drew the outlines of the cross section and constrained them to the correct variables. I also drew a construction line and located it away from the outer line by using variable PipeInnerDiameter/2. 
After that I selected all the lines, selected revolve padding tool, and chose construction line 1 as the axis. 

![Funnel cut section](/images/freecad_funnelsketchconst.jpg)

![Revolve axis](/images/freecad_revolveaxis.jpg)

![finished models](/images/freecad_finishedmodels.jpg)

And then both of my parts are actually finished! 


#### TBC....





 