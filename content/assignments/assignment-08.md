+++
title = "Week 8 : Electronics design"
+++

## Week 8: Electronics design

<!-- Conn_PinHeader_UPDI_2x03_P2.54mmm_Vertical_SMD

What the component is_variant_how many pins rows/columns_p stands for pitch, which is the distance between the pins_alignment -->

This week we have been diving deeper into the world of electronics, and got to learn a lot more about actually desigining the boards ourselves. We learned how to mill two sided PCB's, and how to test our boards using a multimeter, and an oscilloscope. The assignment outline went as follows:


* Use the test equipment at the Fablab to observe the operation of a microcontroller circuit board:
    - check operating voltage on the board with multimeter or voltmeter;
    - use oscilliscope to check noise of operating voltage and interpret a data signal.
* Redraw one of the echo hello-world boards or equivalent and
add (at least) a button and a LED (with current limiting resistor) or equvivalent input and output,
* check the design rules, make it, test it.
* Document your process in a new page on your website.

I have to admit here that I got carried away with designing, milling, and debugging the board and the code, and I didn't have time to try out the testing equipment.

![Image 27: Finished Binary Counter](/images/A8/A8-27.jpg)

## KiCAD - Schematics

The board we needed to redesign was one of [these](http://academy.cba.mit.edu/classes/embedded_programming/index.html#echo) echo-boards. Our instructor recommended to use ATtiny 412 or 1614 microcontrollers, so those were the two I focused on. I started by looking at the boards and trying to figure out what each of the components were, and what were their purpose. [Nadieh Bremer's](https://fabacademy.org/2021/labs/waag/students/nadieh-bremer/blog/week-6/) fab academy website was hugely helpful to me this week when I was learning more about electronics components. She was a last year's student at fab academy, and her documentation was meticulous and informative.

#### ATtiny 412 echo board

![Image 1: ATtiny 412 echo board](/images/A8/A8-1.jpg)

Looking at the above image, we can see that there are 4 basic components on the board. The green text on the image tells us information of the different components, and the red text tells us information about the individual pins. 

* C1 1µF - C stands for capacitor, which connects the VCC pin of the microcontroller to the GND, or Ground pin. The number 1 means the order of the same type of components, so if there was another capacitor that would have C2 in its place, and so on. 1µF is the capasitance of the capasitor, which can be used to calculate the maximum voltage that the capasitor can handle. Capasitors basically store energy, and release it in a steady rate. As used here it filters out any irregularities or 'noise' in the current. Looking at electronic devices I've sometimes wondered how some of them stay on for a couple of seconds even after you've cut the power, and I now realise its probably because of capasitors.

* IC1 t412 - This is our microcontroller, which in this case is the ATtiny 412. The microcontroller is basically the brains of the operation. We can program it to do a task that we want, and it will command the other components that are connected to its various pins. Whereas a lot of the other components can be soldered on whichever way, it is very important for the controller to be soldered on in the correct orientation. VCC and GND pins are important to be connected to other components in the correct way, and the other pins can also have different properties. The datasheets of the controllers are long as there are so many different things they can be used for, and are a vital resource in electronics design. The [MegaTinyCore Github Page](https://github.com/SpenceKonde/megaTinyCore/blob/master/megaavr/extras/ATtiny_x12.md) has also a lot of good information. 

![Image 1: ATtiny 412 pinouts](/images/A8/A8-2.jpg)

The above pinout diagram shows information regarding the pins and their possible use cases. The ones I've gotten familiar with are GPIO pins, which are shown in orange numbers, UPDI-pin for programming, shown in red in above diagram, but can also be seen in the first image, and TX and RX, which stand for transfer and read. Their purpose is to send or receive data.

* J1 FTDI & J2 UPDI - These are two different type of connectors that can be used to program the board, as well as connect it to power or other boards or components. The pins are labeled and need to be connected to the corresponding pins of the microcontroller. The only exception is TX and RX. TX (send data) pin on the connector needs to be connected to the RX (receive data) on the controller, and vice versa. So far we haven't used RTS or CTS pins. At this point I'm also not too sure of the difference between UPDI and FTDI, but it is something I'm planning on looking into.

#### Parts to add

As specified in our assignment for this week, we will need to add at least one LED, with a current-limiting resistor, and a button. This means three more components to add, and figure out where to connect them. 

* Resistor, marked by the letter R in schematics, is used to create resistance in the electrical flow. They are very useful to add to reduce the current for example for LEDs that can't handle high electrical flow. The unit of used to signify maximum resistance is measured in Ohms (Ω). In this case we can check the [datasheet](https://optoelectronics.liteon.com/upload/download/DS22-2004-049/LTW-150TK.pdf) of the LED to see the maximum current which it can handle, which I think is 100mA based on the datasheet. To calculate the units necessary we can use the following formula:

``` bash
R = V / I

R = Resistance
V = Voltage
I = Current

```

We are getting power to the board from a USB port, so we can look there for information about the voltage. Most USB ports supply 5V of electricity, so we will use that for the calculations.

``` bash
R = V / I
5 / 0.01 = 500Ω

```

Therefore if my calculations are correct, the resistor should be at least 500 Ω.

* LED, or light-emitting diode. Diodes have two sides, anode (positive) and cathode (negative). Like the microcontroller, it is important that diodes are connected in the correct orientation. Usually there is some kind of mark on the cathode side of the diode, which indicates that it should be connected the the ground. Diodes let current flow to only one direction, so they can be used to protect circuitboards from accidentally connecting a power source in the wrong direction. They can however also be used to emit light, as is the case with LEDs. 

* Button, or a switch. This is pretty self-explanatory. The button is a switch-type component that can be used to send a signal. The button we used has four pads, two to be connected to a specific 'button'-pin, and two to the ground pin. 

## KiCAD - Schematics

Now that we've gotten familiar with the different components, it's time to start designing! I had never used KiCAD or drawn any kind of schematics before, but it was surprisingly intuitive. The [library](https://gitlab.fabcloud.org/pub/libraries/electronics/kicad) over at the fabcloud public gitlab repository has a component library for KiCAD which includes all of the components included in the official fab inventory. I followed the instructions on the repository to download the library so that the components would be correct when I went to mill and solder the board. 

Our instructor had shown us how to use KiCAD on one of our local lectures, and I watched over again the video to follow along while I was desigining my own board. 

![Image 3: Schematics editor](/images/A8/A8-3.jpg)

KiCAD has a couple different worspaces, out of which I was using mainly two. Schematics editor, and a PCB editor. Schematics is a common term in electronics, and shows kind of a basic plan of the components and connections. By clicking the Add a symbol (A) tool we can start adding components on our schematic. 

![Image 4: Components](/images/A8/A8-4.jpg)

![Image 5: Components](/images/A8/A8-5.jpg)

This is where the fablab library comes in handy, as all the components we are using already have their footprints and other information filled in. 

After figuring out the role of each part and where they are supposed to be connected, filling out the schematics is quite easy, as at this point the position of each component doesn't matter.

![Image 6: Components](/images/A8/A8-6.jpg)

After I had finished adding in all the components, it was time to switch tool to Add a wire (W). Which would show which pins should be connected where. After adding all the wires we need to use Add a no-connection flag (Q) tool to add a cross to pins that won't be connected to anything. 

![Image 7: Wires](/images/A8/A8-7.jpg)

Now we could probably take this to the PCB editor and it would probably work, but it is quite confusing to try and follow with all the crossing wires. This is where we can use Labels (Cmd + L) to simplify the connections. For VCC and GND there are special labels called power ports. They can be added to every pin thjat should be connected to either VCC or Ground. For other connections we can create our own labels. After adding the labels we need to add a special power port, power flag, to show where we are getting the power from. I've added them on the FTDI port VCC and GND pins. Then there are only two things left, annotating the schematic using 'Fill in schematic symbol reference generators' which will replace the question marks of the components with order numbers. Then next to that tool is 'Perform electrical rules check' which will show whether there are any errors we need to solve before moving on to the next phase. 

![Image 8: Schematic](/images/A8/A8-8.jpg)

As you can see above the schematic is much easier to read with the use of labels instead of just wires. The electrical check came back with no errors, so we can move on to the PCB editor!

## KiCAD - PCB editor

![Image 9: Files](/images/A8/A8-9.jpg)

When creating a new project in KiCAD it will automatically create a Schematics file and a PCB file. These are both connected together, so it is easy to bring the components to the editor. 

![Image 10: Update PCB](/images/A8/A8-10.jpg)

By clicking the 'Update PCB with changes made to the schematic (F8)' will bring all the components in to the PCB editor. From there on we can click and drag the components to different positions. Here is where the route the copper traces that will actually connect the pins on the physical board. Traces that aren't supposed to connect can't cross each other, so the the more components you have, the more complicated it will be to figure out the routes. 

![Image 11: Routing](/images/A8/A8-11.jpg)

Using the tool 'Route tracks (X)' we can click on any of the pads that need to be connected, and the program will highlight the pads it needs to be connected to. Then we will do the same for each of the pins. The picture above isn't a very good example, but as it is only a quick routing for the purposes of the documentation I haven't spent much time optimising it. However even with that we came into the problem that currently there is no way to connect the UPDI VCC to the microcontroller VCC. This could definitely be solved with some reorganising of the board, but what if we had a lot more components and pins? We can create a second layer which will be the other side of the board and use holes and rivets to connect the tracks from there. 

![Image 12: Routing 2](/images/A8/A8-12.jpg)

By pressing V we can create a tunnel to the other side od the PCB. The blue track shows the track on the other layer of the PCB. After we are sure everything has been connected correctly we need to add in the outlines of the PCB, by using lines and arcs. After that we are ready to perform design rules check, and if there are no errors we can proceed to plot the gerber files.

![Image 15: Plotting](/images/A8/A8-15.jpg)

For this project we need the Front and Back copper layers, and we will also plot out Front and Back Mask layers and the Cut layers as back up in case we need them. Ticking the option to plot Cut layer on all layers is useful, so we won't have to open and align it separately on CopperCam. 

## Assignment

When I was thinking about what could I do with the assignment, I wanted to make something (semi)functional, and was trying to think what could I do with an LED and a button. At first I thought about making a binary clock, but I figured it would be too difficult. That however lead me to binary counters, and I thought that was a great idea. I wanted to count to more than 1, so I decided to add 4 LEDs altogether, as well as resistors for each and a button. Using KiCAD I drew the schematic and PCB. 

![Image 13: BinCounter Schematic](/images/A8/A8-13.jpg)

![Image 14: BinCounter PCB](/images/A8/A8-14.jpg)

After plotting out the gerber files the steps to mill the boards were very similar to the previous, electronics production week. Important to note that any layers that will be milled after the board is turned over need to be mirrored. This time there were a few additional steps, mainly adding additional layers and aligning them using the same pads as reference. I right-clicked on the through-holes and set the drill diameter for the holes at 1mm, which looking back should've actually been 0.8mm for the smaller rivets. After going through selecting the active tools I generated the toolpaths and moved on to milling. 

![Image 16: Milling 1](/images/A8/A8-16.jpg)

![Image 17: Milling 2](/images/A8/A8-17.jpg)

I etched the traces, drilled the holes, and cut the PCB with the SRM-20 machine, changing the tools between different tasks. After these were done I needed to turn over the board, to be able to etch the other side. While turning it over you should be careful to only turn it over the y-axis, instead of x-axis, which is exactly what I did. This was fine however as my backside had only one short track. I turned the board into the correct orientation and the board was finished milling. 

![Image 18: Milling 3](/images/A8/A8-18.jpg)

![Image 19: Milling 4](/images/A8/A8-19.jpg)

Next I used the 2-sided rivets to connect the two sides to each other. After that I was reasy to collect the components and solder everything together. 

![Image 20: Soldering 1](/images/A8/A8-20.jpg)

![Image 21: Soldering 2](/images/A8/A8-21.jpg)

![Image 22: Soldering 3](/images/A8/A8-22.jpg)

I used solderpaste and a heatgun to solder the LEDs as they are very heat sensitive and can easily be burned with a soldering iron, but otherwise it was very similar to what I've been doing in previous assignments. 

## Coding

To program my previous board I had downloaded Arduino IDE, to connect and upload the code to the board. I used [this](https://www.arduino.cc/reference/en/language/functions/bits-and-bytes/bitread/) function in my code to have my LEDs display the 'count', or how many times the button had been pressed, in binary form. 


``` bash
/* 
 
  Binary counter

  Push of the button increases the count by one, current count shown in binary with the LEDs.
  Max count is 16, next increase resets the counter.

  Fiia Kitinoja
  6 March 2022

*/

int pin0=6; // LSB
int pin1=7;
int pin2=8;
int pin3=9; // MSB
int btn=10;

int count=0;
int btnState=0;

void setup() {
  // put your setup code here, to run once:
  pinMode(pin0, OUTPUT);
  pinMode(pin1, OUTPUT);
  pinMode(pin2, OUTPUT);
  pinMode(pin3, OUTPUT);
  pinMode(btn, INPUT);
  resetLEDs();
}

void loop() {
  // put your main code here, to run repeatedly:
  
  btnState = digitalRead(btn);

  if (btnState == HIGH) {
    count=count+1;
    updateLEDs();
  }
 }

void updateLEDs() {
  if (count >= 16) {
    resetLEDs();
    count=0;
  } else {
    digitalWrite(pin0, bitRead(count, 0));
    digitalWrite(pin1, bitRead(count, 1));
    digitalWrite(pin2, bitRead(count, 2));
    digitalWrite(pin3, bitRead(count, 3));
  }
}

void resetLEDs() {
  digitalWrite(pin0, LOW);
  digitalWrite(pin1, LOW);
  digitalWrite(pin2, LOW);
  digitalWrite(pin3, LOW);
}

```

The first version of my code. After uploading it somewhat worked, however as a random number generator instead of a counter, as you can see from the video. 

{{< video src="/images/A8/A8-25.webm" type="video/webm" preload="auto" >}}

I accidentally set the counter to run when the button was NOT pressed and stop when it was. I fixed the code however ran into a new problem. The switch was turning on whenever i moved my finger close to the button, and I couldn't figure out what was wrong. Unfortunately I wasn't able to get it on video as it was very sporadic and irregular. 

I didn't know if the issue was with my code, or my soldering, and I guessed the latter. I tried to desolder the button to remove it but accidentally removed the copper traces with it. 

![Image 23: Desoldering 1](/images/A8/A8-23.jpg)

![Image 24: Desoldering 2](/images/A8/A8-24.jpg)

I tried to reattach the button using wires, but after being at it for a couple hours I realised it wasn't possible, at least for me. I started over and milled and soldered the board again. I connected it to Arduino and it wasn't working. 


``` bash
/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/Blink
*/

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}

```

Using the above Blink code I tested each of the LEDs and realised I had burned most of them when removing them from the old board. I removed the LEDs and soldered new ones, and tested them again, making sure they worked. 

After that I ran the code again, but it still wasn't working. I found [this](https://docs.arduino.cc/built-in-examples/digital/Button) page and used its code to test the button.


``` bash
/*
  Button

  Turns on and off a light emitting diode(LED) connected to digital pin 13,
  when pressing a pushbutton attached to pin 2.

  The circuit:
  - LED attached from pin 13 to ground through 220 ohm resistor
  - pushbutton attached to pin 2 from +5V
  - 10K resistor attached to pin 2 from ground

  - Note: on most Arduinos there is already an LED on the board
    attached to pin 13.

  created 2005
  by DojoDave <http://www.0j0.org>
  modified 30 Aug 2011
  by Tom Igoe

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/Button
*/

// constants won't change. They're used here to set pin numbers:
const int buttonPin = 2;     // the number of the pushbutton pin
const int ledPin =  13;      // the number of the LED pin

// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status

void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);

  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState == HIGH) {
    // turn LED on:
    digitalWrite(ledPin, HIGH);
  } else {
    // turn LED off:
    digitalWrite(ledPin, LOW);
  }
}


```

I changed the pins in the code to the correct ones in my board, and saw that the button wasn't working. After re-, de-, and resoldering and testing again I got the button to work as well. 

I had gotten advice from my instructor and fellow classmates to use pinMode(btn, INPUT_PULLUP) to fix the issue with static electricity from my fingers, and changed that in my code. That got it working, but instead of counting in order, the counter was showing me random numbers. This was a debounce issue, as the counter was running too fast while I pressed the button. With the help of [this](https://www.arduino.cc/en/Tutorial/BuiltInExamples/Debounce) page and some other research I added some more code, which made the board work correctly!


``` bash
/* 
 
  Binary counter

  Push of the button increases the count by one, current count shown in binary with the LEDs.
  Max count is 16, next increase resets the counter.

  Fiia Kitinoja
  6 March 2022

*/

int pin0=6; // LSB
int pin1=7;
int pin2=8;
int pin3=9; // MSB
int btn=10;

int count=0;
int buttonState;
int lastButtonState=LOW;

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); // open the serial port at 9600 bps:
  pinMode(pin0, OUTPUT);
  pinMode(pin1, OUTPUT);
  pinMode(pin2, OUTPUT);
  pinMode(pin3, OUTPUT);
  pinMode(btn, INPUT_PULLUP);
  resetLEDs();
}

void loop() {
//  count++;
//  updateLEDs();
//  delay(1000);
  // put your main code here, to run repeatedly:
  int reading = digitalRead(btn);
  if(reading != lastButtonState) {
    // Reset debounce timer
    lastDebounceTime = millis();
  }
  if ((millis() - lastDebounceTime) > debounceDelay) {
    if(reading != buttonState) {
      buttonState = reading;
      if(buttonState == HIGH) {
        count=count+1;
        updateLEDs();
      }
    }
  }
  lastButtonState = reading;
 }

void updateLEDs() {
  if (count >= 16) {
    resetLEDs();
    count=0;
  } else {
    digitalWrite(pin0, bitRead(count, 0));
    digitalWrite(pin1, bitRead(count, 1));
    digitalWrite(pin2, bitRead(count, 2));
    digitalWrite(pin3, bitRead(count, 3));
  }
}

void resetLEDs() {
  digitalWrite(pin0, LOW);
  digitalWrite(pin1, LOW);
  digitalWrite(pin2, LOW);
  digitalWrite(pin3, LOW);
}

```

{{< video src="/images/A8/A8-26.webm" type="video/webm" preload="auto" >}}

Above you can see my little counter counting in binary!

As I said above I got so carried away with my little board I completely forgot about using the testing equipment. I had to do so much research for this project and learn so many new things I ended up not having time for it, but I will definitely make time to use the testing equipment next time I am in Fablab and document it here. 

## Oscilloscope

At its heart, oscilloscope is a device for seeing how the voltage of the signal varies over time. 

![Image 30: Oscilloscope 1](/images/A8/A8-30.jpg)

The oscilloscope in Aalto Fablab can display two different analog signals simultaneously. The vertical position knob will change the location of the signal on the screen, and the scale will change the height of the waves. Vertical scale is expressed in Volts per division.

![Image 31: Oscilloscope 2](/images/A8/A8-31.jpg)

All of the channels share the horisontal control parameters. They work the same way as vertical controls, position moves waves left to right and scale changes the width of the waves. However both channels change at the same time. Horisontal scale is expressed in seconds per division. 

![Image 32: oscilloscope 3](/images/A8/A8-32.jpg)

The last of the controls is the trigger. Trigger level is the voltage your signal has to cross through for the oscilloscope to update the display. With rising edge trigger you want your trigger level to be somewhere between the top and bottom of your waveform. 



* Use the test equipment at the Fablab to observe the operation of a microcontroller circuit board:
    - check operating voltage on the board with multimeter or voltmeter;
    - use oscilliscope to check noise of operating voltage and interpret a data signal.

