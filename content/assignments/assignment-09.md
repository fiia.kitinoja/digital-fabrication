+++
title = "Week 9 : Computer-controlled machining"
+++

## Week 9: Computer-controlled machining

Assignment:

* Participate one of the introduction sessions or watch instruction videos.
* Pay attention to what runout, alignment, feedrate and tool paths are.
* Design, CNC mill and assemble something big (meter scale). You will get a 1200x1200x18 mm sheet of plywood for your assignment. Make use of it.
* Document your process in a new page on your website.
* Add downloadable design files to your documentation page.

This week's machine is by far the most intimidating that we've tackled so far, and for that reason I wanted to take it slow and explore the machine a bit before diving in to designing and cutting my large scale object. I've been building a macropad to help me with my workflow, and I wanted to use the CNC machine to carve a case for it out of wood. I was able to use leftover pieces to make it, which is great because now after I've used the CNC machine a bit and have a better grasp of it I'm more confident about designing something bigger for it and using it to cut the parts. 

![Image 6: Macropad first version](/images/A9/A9-26.jpg)

Here is the very first version of the case that I cut, and as you can see it didn't come out that well. I had used some leftover wood that I found on the workshop. After that I decided to try some test cut first to better understand the machine. And that helped me get a better result at my next try. 

![Image 1: Macropad](/images/A9/A9-1.jpg)

![Image 2: Macropad case](/images/A9/A9-2.jpg)

Above you can see the current state of my macropad, as well as the case carved by the CNC machine. Next I am planning on making a small endtable for my couch. Anyway below I will talk about the process this week.

## Recontech 1213 CNC milling machine

![Image 3: CNC Machine](/images/A9/A9-3.jpg)

The CNC machine is one of the most, if not the most dangerous machine in the lab, so caution should be used whenever operating it. It is inside its own room, and it is only possible to be turned on if the door is closed, to increase safety. 

#### Milling tools

There are different types of tools for the CNC machine. Drilling tools are mainly used for drilling, or making holes of the tools specific diameter, but not for milling. We are focusing on the milling tools, which are better at removing material from the sides, not so much vertically. 

![Image 4: Drilling and milling tools](/images/A9/A9-4.jpg)

There are a lot of different types of milling tools, which are identified through a 4-digit serial number. The first number of the serial number usually signifies the diameter of the tool, however are other aspects that need to be considered when choosing a tool to use for milling. In fablab there is a sheet containing information on all the milling tools used for the Recontech CNC machine, and there are 8 categories of information:

* Tool name
* Type
* Serial no.
* Flute Diameter
* Flute length
* Number of flutes
* Shank diameter
* Full length

![Image 5: Tool diagram](/images/A9/A9-5.jpg)

![Image 6: Tool info sheet](/images/A9/A9-6.jpg)

Sometimes things might not be put in the right boxes, so it is always a good idea to double check the measurements of the tool with a caliber to make sure you have the correct one. 

The types of the tool varies, but the two most common types are downcut and upcut. The names imply the direction of the cut. Upcut tools clear away the chips, and are therefore best for rapid cuts, and allow a good finish on the bottom of the part. Downcut tools press the material down to the table, so they are best used for thinner materials and for getting the best finish on the top part. 

#### Setting up for toolpaths

Before we are able to create toolpaths for the machine there is specific information we need to know, which are material information, and tool information. For the tool we need to know the **flute diameter** and the **number of flutes**. Regarding the material, we need to know its **x, y and z dimensions**. You should always measure the dimensions yourself, as there can be compression or expansion happened to the stock. For Digital Fabrication each of us will get a 1200 x 1200 x 18 mm piece of pine plywood to work.

#### Tool selection

It is important to select the tool you will be using before creating toolpaths, as you will need the tool information to figure out settings for the toolpaths, such as feed rate, radiuses, and others. I wanted to test the difference between upcut and downcut tools before I cut my case, and decided 6 mm diameter would be best because my case was quite small. When I will be cutting some bigger items I think 8mm or even 12mm will be better, as they will be much faster than the 6mm.

![Image 9: Tool drawer](/images/A9/A9-9.jpg)

![Image 10: 6 mm downcut 2 flute](/images/A9/A9-10.jpg)

#### VCarve Pro

VCarve Pro is a software used to create toolpaths for CNC machines, and is the program we were shown how to use on our introduction to the machine. It has simple 2D drawing tools included, so it can be used both to create shapes from scratch and then use them to create toolpaths for the machine, or import 2D vector files from other program such as AutoCAD, Illustrator, Fusion360, and so on. The program feels quite intuitive to use, and not too complicated. Its drawbacks however is that its 3D capabilities are limited compared to Freecad or Fusion360. It is possible to create toolpaths in Fusion360 as well, but the process is more complicated as there are many more actions to be taken and fields to be filled. 

![Image 7: Job setup](/images/A9/A9-7.jpg)

When creating a new project in VCarve, the job setup window will open up. This is where you setup you 'canvas' aka your stock dimensions. It is important to measure the thickness of the material with a caliber, as for example the leftover piece of 15 mm birch plywood was actually 14.62 mm as you can see from the screenshot. When cutting you only need to set the cutting depth about a quarter of a millimetre over the cutting depth. This will help extend the lifespan of the spoilboards, which are placed under the material to protect the machine bed. Other important factors is to make sure the Z Zero position and XY positions are correct. For this week we have been using only machine bed as the Z position. We were also told to make sure the Use Offset box was unticked, as that is a more advanced feature. 

![Image 8: Creating a shape](/images/A9/A9-8.jpg)

For testing purposes I used the drawing tools in VCarve to create a rectangle. I set the width and heigth at 75 mm so that I would be able to measure them after cutting and see how accurate the cut was. This would be important in creating joints that fit perfectly. I set the corner radiuses to half the width of my tool, so 3mm. Because the milling tool is round, it is not capable of cutting sharp corners. For this stage it is not a problem in my objects, but can create a problem with joints. There are tools to help design around that and make joints that fit together perfectly, called boning, but I will cover that later in this page. 

![Image 11: Creating a toolpath](/images/A9/A9-11.jpg)

After creating the rectangle we want to set up our first toolpath to cut it out of the plywood. On the toolpaths menu we clicked 2D Profile Toolpath, which tells the machine to cut along the selected line. A new window will appear on the left panel showing different options for the settings for the toolpath. 

**Cutting Depths** tells the machine how deep to cut. The start depth tells the machine where to start, and should always be at 0 for the first pass. However it can be useful to set it lower if previously there has been cuts, such as a pocket for example. Then you would set the starting depth of the new cut to the depth of the previous cut. Cut depth implies the point how deep the cut will be. Ive set mine here at 14.85, whic is 0.23mm deeper than the thickness of my material, because I wanted to cut through it. If you are cutting through make sure you have a spoilboard underneath so that you don't damage the machine bed. 

![Image 12: Tool settings](/images/A9/A9-12.jpg)

**Tool** setting is where you either select the tool or add the information of it if it is not filled out already. After selecting the correct tool, 6mm end mill in my case, I will have to set the other parameters. The pass depth determines the depth of each cutting layer, and can have an impact of the quality of the cut, while sacrifising speed, or other way around. I set mine at 3mm, half of the diameter again, as we were told this was a good rule of thumb. 

Feeds and speeds are important, as it ensures the machine works safely and prevents overheating in the way of stuck chips or something else. 16000 is a good average speed, but the machine is capable of going higher, I need to check what is the maximum. 

![Image 13: Feed rate calculation](/images/A9/A9-13.jpg)

Feedrate is calculated after you have determined the chip load, number of flutes, and the spindle speed. The chip load depends on the tool, as well as the material you are using, and can be checked on the data sheet from the tool manufacturer. In the image above our instructor was using a 8mm tool, which had a higher chip load than what I was using, which is why the feedrate is higher. Number of flutes can be checked on the infosheet on the wall at the lab. Plunge rate should be roughly a quarter of the feed rate. 

**Machine Vectors** After setting the tool settings we can move on. Next step is to determine whether the machine will cut on the outside of the line or the inside of the line. CNC will carve away material roughly the diameter of the cut, and so it depends which edge you want to be closer to the actual dimensions of the design. Usually if you are cutting an object out of the material, you want the cut to be outside, however if you are cutting a hole into an object, you want it to be on the inside. 

The direction of the cut is called Climb or Conventional. These can have different results on the final dimensions as well as the finish quality, so in my testing I used both of them. 

**Tabs** was the final setting I needed to set. When cutting away parts the object would start to move as the machine got close to finishing the cut, and could fly away damaging the machine or people, which is very dangerous. That is the reason we want to add tabs so that the part will keep attached to the material sheet even after the CNC is finished milling. The tabs will be smoothed out in post processing. 

After these settings we were ready to save the toolpath as a text file.

#### Setting up the machine and the material

Next we should set the material on the machine. We were shown how to change the tool on the machine which included moving away the dust collector, placing in the correct size 'holder' (the exact term escaped my mind just now..) for the tool, and placing in the tooltip and screwing it on loosely. Then we needed to use a wrench and a specialised tool to tighen the tip. 

After that we needed to make sure to secure the spoilboard and the material to the machine bed, so that they wouldn't move during the operation. The CNC machine in fablab has a vacuum bed, which makes securing it easy, if you manage to get the vacuum to work properly.  

![Image 14: Vacuumbed](/images/A9/A9-14.jpg)

If set properly, the vacuum will suck the material tightly to the bed which makes sure it doesn't move. Depending on the size of the material, the vacuum area can be adjusted by opening up the airholes, and using rubber tubes to fill the gridded area to create a closed, airtight space. The spoilboard material we use at fablab is MDF, which lets air through. 

![Image 15: Vacuum dial](/images/A9/A9-15.jpg)

The dial at the bottom of the machine shows the pressure of the vacuum. The dial should point roughly to the middle of the green area, which means the material should be well secured. If the pressure isn't strong enough, as in the picture above. the rubber bands and airholes should be adjusted to make sure more air isn't getting in. 

#### Mach3

Now we are ready to start Mach3, which controls the CNC machine. The CNC should be powered on before starting up the program, otherwise an error message will show up stating that it is not connected. After the program is running there are three things that need to be done. 

* Click reset
* Make sure soft limits is green
* Click Ref all home

The last button will have the CNC move XYZ axis to its home position, so you should be careful you aren't standing very close to the machine. 

![Image 16: Reset Z axis](/images/A9/A9-16.jpg)

After this the next step is to reset the Z axis. This can be done automatically, by putting the small device on top of the spoilboard, directly under the tooltip. The reason why it is not put on top of the material is because we set our Z origin to be on top of the bed. Make sure the vacuum is on so that there is no gap between the machine bed and the spoil board, as that could give a wrong value. After setting the device below the milling tool we can click the button on Mach3 called 'Terän mittaus' (Measuring the blade).

Next we can put the device back and use the page up tool to raise the tool from the surface of the bed. The arrow tools can be used to move the machines x and y axis, and after moving the tool to the correct x and y origin we click zero x and zero y to reset their origins. 

Finally the last step is to click load G-code, select the toolpath file, and get ready to start the cutting. 

![Image 17: Machine control from the outside](/images/A9/A9-17.jpg)

In the fablab there is a safety measure that prevents starting of the cutting process if the door is open, so we need to leave the room and close the door. Then we can click the big green button which starts the cutting proces.

#### Testcuts

![Image 18: Machine control from the outside](/images/A9/A9-18.jpg)

I tested two different tools, as well as a few different settings. The bottom two cuts and four pockets were done with the 6mm downcut tool, and the rest with 6mm upcut tool. As you can see the difference in the quality of the cut was huge. 

I also tested climb vs conventional cutting on both tools, which didn't result in that big of an observable difference. The main difference was in the dimensions, as you can see the climb setting removed slightly less material than the conventional. 

Finally the last setting that I tried was raster vs. offset, which determines the path in which the tool will clear away material in the pocket. Below you can see the difference in the paths. The main difference between them was time, as the offset pockets took 1 min 40s each to cut, and the raster ones took 2 min 2s. With such small pockets the difference is small but could be more important with bigger pieces. 

![Image 19: Offset vs raster path](/images/A9/A9-19.jpg)

#### Other cuts

![Image 20: Case](/images/A9/A9-20.jpg)

After the test cuts I opened an illustrator file of the case and cut it using the 6mm downcut, conventional and raster settings.

![Image 21: Case Closeup](/images/A9/A9-21.jpg)

Closeup of the case.

![Image 22: Test cuts closeup](/images/A9/A9-22.jpg)

Closeup of the testcuts.

![Image 23: Case Back](/images/A9/A9-23.jpg)

![Image 24: Case Back 2](/images/A9/A9-24.jpg)

The material I was using must have been slightly warped, as the case wasn't cut out evenly. This wasn't a problem however as the left over bit was paper thin and I was able to break it easily.

#### Post-processing

![Image 25: Router](/images/A9/A9-25.jpg)

After breaking the tabs they needed to be sanded down, which is very easy to do with a router that is available at fablab. With the router I was able to also clean the bottom side of the case. 

## Meter-scale project

![Image 30: joint tests](/images/A9/A9-30.jpg)

![Image 29: End table model](/images/A9/A9-29.jpg)

As my large-scale project I decided to make an end table for my home. I started by testing some different kinds of joints. After that I made a full scale model on Fusion 360. 

![Image 31: Joiner test](/images/A9/A9-31.jpg)

I cut out small section of the model to CNC out and to further test the joints before fully cutting out the pieces. They fit together well so I carried on to cut the full size pieces. 

![Image 32: CNCd pieces](/images/A9/A9-32.jpg)

![Image 33: CNCd pieces](/images/A9/A9-33.jpg)

Cut out pieces. I removed tabs and did a lot of sanding to make the pieces look nicer. 

![Image 35: Wheels](/images/A9/A9-35.jpg)

I purchased some 40mm wheels and attached them to the bottom of the table.

![Image 34: Assembled table](/images/A9/A9-34.jpg)

Above you can see the assembled table. To fully finish it it still needs to be painted and varnished. Carving on the top ended up revealing partly the darkened/glue layer, so I'm planniong on painting some nice pattern on top to cover that. 

## Design Files

* [MacroPad AI](/DesignFiles/A9/EndTable.f3d)
* [End table F3D](/DesignFiles/A9/EndTable.f3d)
* [End table DXF](/DesignFiles/A9/EndTableV2.dxf)