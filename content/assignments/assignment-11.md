+++
title = "Week 11 : Molding and casting"
+++

## Week 11 : Molding and casting

I have been excited about this week for a while, as molding and casting using resin and silicone is something I've been wanting to try out for a while, but for some reason I haven't tried it. I've been watching a lot of channels of makers in youtube using resin, so I've heard some tips and tricks here and there, but getting some hands on experience will be a totally different thing regardless. 

This week our assignment is:
* Review the safety data sheets for at least two of labs molding and casting materials.
* Make and compare test casts with each of them.
* Design a 3D mold around the stock and tooling that you'll be using.
* Mill it (rough cut + (at least) three-axis finish cut).
* Use it to cast parts.
* Include a hero shot and source files of your design in your documentation.

## This week's project

The first task for me for this week's assignment was to figure out what the object would be, that I wanted to mill with the MDX-40 milling machine. Whereas during the 3D printing week our task was to design something that *can't* be made subtractively, this week it has to something that *can*. The restrictions depend somewhat on the specifications of the machine you are using. I think there are some fancy CNC mills where the tooltip can rotate to different sides of the material, and you can do more intricate stuff with those machines. However the machine we have at fablab only works from the top down into the material, and therefore the main consideration is to make sure the tooltip can reach everywhere where material needs to be removed.

![Image 1: Subtractive manufacturing constraints](/images/A11/A11-1.jpg)

Above you can see a sketch illustrating the main constraint in subtractive manufacturing with the machine in Aalto Fablab. In the example on the left, the tool is able to remove all the necessary material. On the example on the right, the tool can't remove the material under the overhang without also removing the material above, leaving just a straight wall. 

With this in mind I needed to figure out what I wanted to design to be eventually casted in resin. The process of molding and casting is essentially just reprodusing shapes with a different (or the same) material, so before we can actually dive into using silicone we needed to create the object used for the mold. Ever since I got into mechanical keyboards I've wanted to be able to take it to the next step and fully customise my setup. In [Week 9]("{{ .Site.BaseURL  }}assignments/assignment-09") I already showed some of my process in making a case for a mechanical macropad, and this week I really wanted to finally try making my own keycaps! 

![Image 2: Current keyboard](/images/A11/A11-2.jpg)

Above you can see my current keyboard setup. The main thing with making your own keycaps is that they obviously won't be labeled. It won't be a problem for me as I already have blank keycaps. I really like the minimal look it gives and I tend to switch the keyboard language between Finnish and Irish based on what I'm doing in the computer, so I've kinda had to learn where all the symbols are and so on. It's also made my typing considerably faster when I can't look down on the keyboard at all. Anyway enough rambling about keyboards, if anyone reading is interested in getting into the hobby, [r/MechanicalKeyboards](https://www.reddit.com/r/mechanicalkeyboards) is a great resource for more information. Be warned though, before I got my current keyboard I was very happy with using any keyboard and didn't care for the quality, now typing on my laptop keyboard feels like torture. 

There is a huge community making their own keycaps, and a wealth of information about the process. An individual known as Zappycobra has created a modular platform for creating your own artisan keycaps, known as [Z-Butt](https://github.com/imyownyear/Z-Butt). All of the components can be 3D printed to use to make silicone molds. Part of our assignment this week is to design something ourselves to be milled from wax to make the silicone molds, so I won't use these in the assignment. However I couldn't help myself so I did use them initially to test the silicone and resin in the fablab. I will talk about them more below when I get to the actual process of casting and molding. 

![Image 3a: Macropad](/images/A9/A9-1.jpg)

For the assignment I decided to make a rotary encoder knob. As you can see from the photo above, I bought some silver metal knobs for the macropad, but wouldn't it be cooler if the knob was a tiny potted cactus plant? I definitely think so. So I took some measurements and got on to designing the knob in Fusion 360.

## Designing the 3D object

![Image 3b: Parameters & Initial sketch](/images/A11/A11-3.jpg)

Above you can see all the parameters I ended up defining, and the initial sketch for a section for the pot. I'm still new to parameters so I'm sure there is a way to use constraints more efficiently and with less defined parameters, but oh well. 

![Image 4: Revolve & Fillet](/images/A11/A11-4.jpg)

I used the revolve tool to create a round 3D pot of the sketch, and used fillet to round the hard edges of the lip. 

![Image 5: Form](/images/A11/A11-5.jpg)

Next I needed to focus on the cactus. I hid the body of pot, and selected create form tool. This tool helps you create more organic forms than the more geometric design space of Fusion. I created a sphere, and made the design symmetric on 5 sides. I then selected three middle secting of one of the vertical edges, and selected Edit form tool under Modify dropdown menu. I was then able to pull them inwards. This created a nice cactus shape as you can see above. (Side note it took me a lot more trial and error but the nice thing of writing the documentation afterwards is you can skip all of those steps...)

![Image 6: Cactuspot](/images/A11/A11-6.jpg)

Then I just unhid the pot and the base of the knob was ready! I duplicated the shapes and created a folder for the separate bodies to keep the originals. Then I combined the duplicated objects into one body. 

![Image 7: Bottomhole](/images/A11/A11-7.jpg)

Then I needed to make the bottom hollow to be able to fit it onto the rotary encoder. The metal knob in the encoder is 6mm in diameter with a notch, making the overall thickness just slightly above 4mm. The tooltip we are using this week to practice with is 3mm in diameter, so I decided that I would 3D print an adapter for the knob that I would attach to the final resin cast. There are smaller tooltips, but they can break very easily, so its good to practice the use of the machine with the thicker tools. 

![Image 8: 3D printed prototypes](/images/A11/A11-8.jpg)

I used my 3D printer to print a protoype mostly because I was impatient and just wanted to see what it would look like in real life. And it actually gave me some good information on the proportions! The first model I printed (on the left) ended up being too tall and not wide enough. I wanted it to still be more knob-like, so I squished the model down a bit and made it wider (thank you parameters!) and printed version 2 (on the right), which I like a lot more.

![Image 9: 3D print layers](/images/A11/A11-9.jpg)

Now I could technically use this 3D print to make a silicone mold to cast it in resin, but 1) part of the assignment is to use the CNC mill to learn to create 3D toolpaths and, more importantly, 2) the quality of the result will be better using the wax mold. As you can hopefully see on the closeup image above, due to how 3D printing works the surface won't be completely smooth. I used the highest quality setting when I sliced the model to be 3D printed, and you can still see the layers, especially at the top. With 3D milling I should hopefully be able to get a completely smooth surface. 

## Negative and Positive molds

The molding and casting process this week includes three stages.

* Milling a positive mold out of wax using MDX-40 CNC mill
* Casting a negative mold out silicone
* Casting the final object in resin

In this case the terms positive and negative refer whether the shape of the final object is created within the material (positive) or or in the empty space the material leaves (negative). In order to start with the first step we need to have the 3D model prepared. Preparing it includes the stame three stages, with the steps reversed. Therefore the first step was to create the object itself, and next create the negative mold. 

![Image 10: Negative top](/images/A11/A11-10.jpg)

The widest point of my model is close to the centerpoint of the model. Therefore the best way for me to create the mold was to make it a two part mold, as the model would curve inwards and I wouldn't be able to carve the required shape due to the constraints that I mentioned earlier. I offset a plane to the widest point of the model, and extruded a block using previously defined parameters. The block needs to cover the material everywhere, and represents the silicone mold. The more offset there is the sturdier the mold will be, but it will also use up more silicone (which is expensive..). 

![Image 11: Negative top](/images/A11/A11-11.jpg)

Next I selected the negative top and the object, and cut the shape of the object out of the mold. Make sure you have 'keep tools' ticked if you haven't cloned the object. 

![Image 12: Negative bottom](/images/A11/A11-12.jpg)

Next I retraced the above steps for the negative bottom mold. 

![Image 13: Positive and negative](/images/A11/A11-13.jpg)

Final step of drawing the silicone mold was to inclkude some small knobs that would help the mold sit in the correct position while casting, and a small sprue channel, that would be used to inject the resin while the mold is closed.

![Image 13b: Positive mold](/images/A11/A11-13-2.jpg)

The creation of the positive mold is very similar to the negative mold, if not the exact same. A block according to the stock dimension parameters should be extruded, with the top of the block extruded up to the 'bottom' (flat side) of the silicone mold. The thickness of the block should then be calculated and ensured that there is enough space to cover the other side of the negative mold. Then by selecting the block and the silicone mold the shape of the negative mold w3ill be cut out of the block, leaving in a positive mold. 

Finally when the process has been repeated for both sides, the model can be exported as an .STL.

## Milling - Testing toolpaths with foam

The wax material we are using to carve our molds is hard to source, and to minimise waste and to ensure everyone has material to use for this week's assignment we were instructed to use foam to test our 3D toolpaths on, before using the actual wax. 

The preparation and milling process is the same in this case regardless if the material is wax or foam. First we need to prepare the material we are using. Similar to the CNCing process, we need to know the dimensions of the stock material we are using to extract pieces off of. In order to get a high quality finish, the top and bottom faces need to be level. If this isn't the case we need to prepare the material by removing the excess from the top using a 22mm wide tooltip called the butterfly mill.

![Image 14: Z dimension](/images/A11/A11-14.jpg)

 Measure the x and y dimensions of the stock using calibers and the Z dimension from at least two diagonally opposite corners. If the dimensions aren't even, deduct the smaller dimension from the bigger one, and the difference is how much material is needed to remove from the top. 

![Image 15: Tools](/images/A11/A11-15.jpg)

There are three different types of tooltips we have used so far with the MDX-40 machine. Flat end, round end and butterfly mill. The butterfly mill is the only one that comes only in one size, 22mm, and only has one job: evening out to a flar surface. The other tooltips come in two different thicknesses and lengts: 6mm and 3mm, and short and long. 6mm is generally used for rough cuts, as it is able to remove more material faster, and isn't as prone to breaking as the thinner tool. 3mm is used for smoothing out the surface and bringing out the detail of the design. Depending on the depth of the pocket you are milling, choose whether to use the shorter or longer tooltips. 

![Image 16: Securing stock to table](/images/A11/A11-16.jpg)

The stock material should be secured to the table using double sided tape. With the machine in Aalto the stock shouldn't be placed directly in the corner, but slightly towards the center, to ensure the machine has enough space to move and can reach all the area of the stock material.

![Image 17: Tooltips](/images/A11/A11-17.jpg)

![Image 18: Wrenches](/images/A11/A11-18.jpg)

Next after selecting the correct tooltip for the job at hand select the appropriate size collet. The shank of the butterfly mill is 6mm, so it will use the same collet as the 6mm tooltips. Tightening the collet can be done by using two wrenches of 10mm and 17mm in size. Installing the tooltip is a bit trickier than in the CNC machine, as you need to make sure the collet is tight enough to hold on to the tool tip without it falling, but not too tight so you are still able to insert the tooltip in. Screw collet in, and check if the collet is strong enough to hold the tool. If not take it out and tighten the collet a little bit, so that you can insert the tool, and when you let go it wont fall, so that you can tighten it all the way using two hands.

## Generate toolpaths in VCarve Pro: Surface leveling

![Image 19: VCarve-JobSetup](/images/A11/A11-19.jpg)

Next you are ready to open VCarve Pro and generate the 3D toolpaths. The process again is quite similar to the CNC process. Setup the job by using the dimensions of the stock you are using, measured with a caliper. However, differing from the CNC process set Z zero position to **Material Surface**. As the thickness use the bigger dimension you measured from your stock. 

![Image 20: VCarve-JobSetup](/images/A11/A11-20.jpg)

Next draw a rectangle that covers the whole area of the stock. 

![Image 21: VCarve-PocketToolpath](/images/A11/A11-21.jpg)

Next select the rectangle, and click Pocket toolpath on the toolpaths window. Set the difference you measured previously as the Cut depth. In order to get the corners set pocket allowance to -11mm (or half the diameter of the cutting tool's diameter).

![Image 22: VCarve-Tool selection](/images/A11/A11-22.jpg)

Choose the correct tool and the correct settings. For flattening the surface I chose 22mm end mill. For foam pass depth can be 2mm, but for wax it shouldn't exceed 1mm. The max spindle speed of the machine is 15 000, which is why 14 000 is a good choice as it is slightly below the maximum. To calculate the feed rate we will use the same formula as for the CNC machine. 

``` bash
fr = nf * cl * ss

2 * 0.2 * 14 000 = 5600

Plunge rate 5600 / 4 = 1400
``` 
![Image 28: Chip load](/images/A11/A11-28.jpg)

Chip load can be checked from the manufacturer's website. There is no foam as a reference material, so in this case I used MDF as a reference. The tool diameters also only go up to 12mm, but we can roughly estimate the chipload by doubling the value for a 12mm tool, which is 0.12, so we set the chip load at 0.2. 

The machine can't support a 5600 feedrate, as the maximum feedrate is 3000. This is fine, as the feedrate can easily be lower than the suggested value from the formula. I set the feedrate to be 2500, as again we will go slightly lower than the max. 

With that the toolpath settings are done, we can give it a name and save it on the computer to be executed. 

## Controlling the MDX-40

![Image 23: Roland VPanel](/images/A11/A11-23.jpg)

Roland VPanel is the software used to control the MDX-40 machine. After opening the program the first step should be checking the settings. Click setup on the lower left corner, and make sure command set is set to NC code. Next click OK and make sure that the coordinate system is set to G54. After that the tool can be moved using the arrows on the screen. 

![Image 24: Resetting Z axis](/images/A11/A11-24.jpg)

To reset the z origin take the sensor device, make sure its properly connected and put it under the tool. On the Vpanel select set Z origin using sensor and hit detect. Next move tool to correct XY origin point, select set XY origin here and click apply.

![Image 25: Override](/images/A11/A11-25.jpg)

Next check override settings. If they are 100%, it will use the exact settings from VCarve.
It is a good idea to start slowly, at 10 or 50% and increase when you see that there are no issues and the machine can handle the feedrate. You can increase the values in real time while the machine is cutting.

Then click cut, remove previous toolpaths files, select the correct toolpath file, and click output. Machine will then start.

![Image 26: Leveled stock](/images/A11/A11-26.jpg)

Now if everything went correctly you should have a even leveled stock. If the other side of the stock is also uneven, you might have to flip the material and repeat the process.

## Generating 3d toolpaths

Next we can move on to the actual 3D milling process. 

![Image 27: STL model in VCarve](/images/A11/A11-27.jpg)

Import 3D model (STL file), and check that model size is correct. Select zero plane position in model, make sure everything that needs to get milled is above the zero plane. Select OK. Everything beneath the zero plane will be discarded if you tick the box.

Based on the import settings the model will be adjusted. Next open toolpaths. Setup material. Green box will show to represent material. Use model position setup to select position (usually top of stock piece). Select roughing toolpath and select the settings.

Click okay and make a roughing toolpath.
Check chip load for MDF (6mm)
14 000 for spindle speed.

fr = nf * cl * ss

2 * 0.07 * 14 000 = 1960
Plunge rate 1960 / 4 = 490

Foam pass depth is 2mm, for wax 1mm
for stepover 40% is okay
Machinig limit boundary select Model boundary
Machining allowance means the amount of material left between the model surface and the rough cut. The fine tools should have something to cut for smoother finish.
For roughing strategy choose Z level.

Name the toolpath

Next change the tooltip to the correct one, 6mm flat end in this case. Reset the Z-axis, make sure not to change XY-axis. 

![Image 29: 3D toolpath](/images/A11/A11-29.jpg)

Next finishing toolpath.
Ballnose 3mm diameter.
Pass depth, the smaller the smoother (0.1mm)
Make sure you select mm/minute, not second.

![Image 30: Finished foam test piece](/images/A11/A11-30.jpg)

Now we have a finished foam piece! As the settings were successful we should now be able to use the same toolpath files for cutting the wax, as long as we make sure the passdepth is set to 1mm.

Unfortunately I didn't have time to finish the milling process with the wax the week of the assignment, as I had a field trip coming up that took me away from the lab. I will add photos of the final carved mold at the end of the documentation. 

## Casting silicone and resin

While I was waiting for my turn on the MDX-40 mill, I was excited to try out the silicone and resin to get a feel for using them. I started by looking at the datasheet for the two materials that I was going to be using, as was specified our assignment for the week. The two materials that I was using were [Smooth-on Mold Star 15 SLOW](https://www.smooth-on.com/products/mold-star-15-slow/) and [Smooth-Cast 305](https://www.smooth-on.com/products/smooth-cast-305/). 

Different silicones and resins will have different properties, which is why it is always important to familiarise one's self with the safety datasheet of the substance being used prior to actually using it. The silicone comes and is stored in two separate containers, part A and part B, which will be mixed together following the ratio specified by the manufacturer. Each of these parts will have their own datasheet. Below I will go through some of the more important information that can be checked in the datasheet.

* Classification of the substance or mixture.
* First Aid Measures.
* Fire-Fighting Measures
* Accidental Release/Spill measures
* Handling and Storage
* Exposure controls
* Stability and Reactivity
* Disposal Considerations

Based on this information for the silicone and resin, we can find out that the silicone is considered slightly 'safer' substance of the two, as it is not considered a hazardous substance, unlike the urethane resin of the Smooth-Cast. However care should be exercised when using either of the materials. The resin is also slightly more toxic to breathe in, however if used in a well-ventilated area, such as the woodworking room in the Aalto fablab one should be fine without additional respiratory protection. Use plastic gloves for handling substances and do not get in eyes, skin or on clothing. 

After reading through the safety datasheet we can try and use the materials. 

![Image 31: Z-Butt system](/images/A11/A11-31.jpg)

As mentioned in the beginning of this documentation I wanted to use the Z-Butt system to try and cast some keycaps. It includes different parts needed in the mold making and casting process, and I have ordered the 3D printed the parts previously. For this part I'm using the parts shown above, to take a full advantage of the system go look at their documentation on the Z-Butt Github page. 

I wanted to try andn make a sculpted artisan keycap. For the sculpting process I used some Fimo polymer clay I had at home. The advantage of polymer clay is that it wont harden just with coming in touch with air, it may dry out a little but if can be softened with using some baby oil as it is an oil based plastic clay. To harden it should be put into oven at about 130 degrees (temperature varies between brands). That means it will last a long time, but in this case I won't be able to harden it as I can't put the PLA-based sculpting base in the oven. For this reason clays that harden in room temperatures but which soften quickly in hotter temperatures such as Monster clay are recommended for keycap sculpting. 

![Image 32: Sculpt](/images/A11/A11-32.jpg)

To keep in theme with my plant-based keyboard I decided to make a little succulent. I placed the sculpting base and the stem cavity into the container to wait to be covered in silicone. 

![Image 33: Weighing water](/images/A11/A11-33.jpg)

Silicone is very expensive, and a great way to figure out how much of the wet silicone you need to mix is to fill your molds first with water, then pouring the water into a plastic cup and weighing the amount of water in the cup. I found I needed roughly 42 g of silicone to create my mold. 

![Image 34: Silicone parts](/images/A11/A11-34.jpg)

This is what the mold star 15 tubs look like. Yellow having the Part A, and blue/white having the part B. They need to be mixed with a 1 to 1 ratio, and after mixing they have about a 50 minute working time, which leaves plenty of time to get everything correct. The yellow tub on the left is Silicone thinner, which helps bring down the viscosity of the mixture. This will make the silicone easier to pour out, and more importantly easier to get bubbles out. 

![Image 35: Measuring silicone](/images/A11/A11-35.jpg)

I measure equal amounts of Part A and Part B, and about 5 or so grams of the silicone thinner. 

![Image 36: Mixing](/images/A11/A11-36.jpg)

The parts need to be mixed thoroughly and scraping all edges and the bottom to make sure to get all the substance mixed in. The mixing will trap in air in the silicone, and to remove them the cup can be placed in a vacuum chamber, which will pull most of the air out of the mixture. 

![Image 37: After pour](/images/A11/A11-37.jpg)

After degassing the silicone, it can be poured into the molds. The pouring should be done in the corner of the mold, with a thin, regular stream to minimise the possibility of trapped air. After the pour the molds can be left to cure. 

![Image 38: Stem cavity demold](/images/A11/A11-38.jpg)

After 4 hours the silicone can be demolded. The stem cavity came out perfectly!

![Image 39: Sculpt demold](/images/A11/A11-39.jpg)

The sculpt wasn't as successful however. As you can see in the image, The silicone managed to get in between the clay, which slightly screwed up the mold. The silicone was also still quite wet, and originally I thought that there had been a reaction with the polymer clay. However, after watching [this](https://www.youtube.com/watch?v=nxiBBRjKba4) video, I realised I just hadn't let it cure for a long enough time. I was too excited and demolded the silicone right after 4 hours, but as explained in the video, the curing times are usually calculated on 100g of material used. When using smaller quantities, the curing can take a lot longer. 

![Image 40: Resin cast](/images/A11/A11-40.jpg)

Even though I know the mold has some issues, I cleaned it up the best I could and used it to cast resin in. The process of mixing resing is the exact same as with silicone. You mix the two parts of the resin according to the manufacturers instructions, mix it, and pour into the mold. The work time of the urethane resin I was using was only about 7 minutes, so I didn't have time to put it in the vacuum chamber. Luckily the mixture isn't as thick as the silicone, and therefore won't retain as many bubbles. 

The holes on the top part of the mold will allow the extra amount of resin to flow out as the top mold is pressed in place. 

![Image 41: Resin demold](/images/A11/A11-41.jpg)

![Image 42: Resin back](/images/A11/A11-42.jpg)

After only 30 minutes the resin was cured and I was able to demold the piece. As you can see on the photo above the cast isn't perfect due to the silicone mold not succeeding great, but also the mold wasn't filled up at the back. This is due to me pressing down too hard on the top mold, and too much resin leaking out from the holes. 

I did the resin cast again on the silicone mold, this time being careful not to press down too hard.

![Image 43: Succulent keycap 1](/images/A11/A11-43.jpg)

![Image 44: Succulent keycap 2](/images/A11/A11-44.jpg)

I sanded down the back and placed the resin keycap on my macropad! It's not perfect, I will remake the sculpt using harder clay, and let the silicone cure longer. However this experiment definitely taught me a lot about using silicone and resin. 

## Covid-break

After coming back from my field trip, I caught COVID and wasn't able to visit the Fablab for about two more weeks to ensure I wasn't spreading the disease around to anyone. 

![Image 45: Wax mold top](/images/A11/A11-45.jpg)

![Image 46: Wax mold bottom](/images/A11/A11-46.jpg)

After being able to come to fablab again I was able to use the MDX-40 to carve the positive molds out of the wax.

![Image 47: Poured silicone](/images/A11/A11-47.jpg)

I poured in some of the Mold Star 15 silicone to make the negative silicone molds. 

![Image 48: Silicone molds 1](/images/A11/A11-48.jpg)

![Image 49: Silicone molds 2](/images/A11/A11-49.jpg)

Removed silicone molds.

![Image 50: Resin cast 1](/images/A11/A11-50.jpg)

![Image 51: Resin cast 2](/images/A11/A11-51.jpg)

Liquid plastic inserted into the mold. I found out there was no needles or other apparatus I could use to insert the resin into the mold, so basically my mold design was doomed to fail. The hole on the side is not big enough to be able to pour in the resin. I tried to add in as much resin as possible, but it still failed, as you can see in the photo above. 

![Image 52: Final cast 1](/images/A11/A11-52.jpg)

![Image 53: Final cast 2](/images/A11/A11-53.jpg)

Here you can see the final cast. There were some bubbles left that you can see in the photo, but other than that the finished quality seems great. I will need to rethink my mold a bit, but I'm looking to try again to make the knob.

## Design Files

* [CactusKnob F3D](/DesignFiles/A11/CactusKnob.f3d)
