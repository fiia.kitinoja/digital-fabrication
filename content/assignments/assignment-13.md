+++
title = "Week 13 : Machine building"
+++

## Week 13 : Machine building

This week's assignment actually lasted for two weeks, as we had the Easter holidays. The task was to divide into groups, and work on building a machine. The specific instructions were:

* Design a machine that includes mechanism, actuation and automation.
* Build the mechanical parts and operate it manually.
* Actuate and automate your machine.
* Document the group project and your individual contribution.
* Include a hero shot and link to machine repository in your documentation.

Originally I wanted to work on a knitting machine, but as in the beginning of the two week period I was still very much suffering from corona and had some catching up to do with other week's assignments I decided to join someone else's group instead. [Anna](https://yeshouanna.gitlab.io/aalto-digital-fabrication-2022/) wanted to work on a linear axis machine, that would be turned into an automatic cat litter cleaner, and I decided to join that group. 

I wasn't able to physically make it into the lab until Monday 18th, and so decided to focus on CAD drawings, as I thought that would be the best way for me to contribute. 

## PoopScooper

![Image 0: Hero Shot](/images/A13/A13-0.jpg)

<!-- {{< video src="/images/A10/A10-V3.webm" type="video/webm" preload="auto" >}}  -->

[Machine Gitlab page](https://gitlab.com/aaltofablab/machine-building-2022b)

Group:

* [Anna Li](https://yeshouanna.gitlab.io/aalto-digital-fabrication-2022/)
* [Yuhan Tseng](https://yuhantyh.gitlab.io/digital-fabrication/)
* [Fiia Kitinoja](https://digital-fabrication.fiikuna.fi/)
* [Samuli Kärki](https://ambivalent.world/)
* [Karl Mihhels](https://karlpalo.gitlab.io/fablab2022/)

## The Process

We had our initial meeting Tuesday the 12th to divide tasks and figure out how to get started. 

![Image 1: Initial meeting sketch](/images/A13/A13-1.jpg)

![Image 1b: Sketch 2](/images/A13/A13-1b.jpg)

![Image 1c: Sketch 3](/images/A13/A13-1c.jpg)

![Image 1d: Sketch 4](/images/A13/A13-1d.jpg)

The basic shape of the machine would be a box in the middle that holds the cat litter. There would be two threaded rods on the y-axis either side of the box that move a rod on the x-axis. A rake like scoop would be connected to that rod and when it moves it would collect the bigger particles of the poop, and let through the smaller particles of the clean litter. 

![Image 2: Box](/images/A13/A13-2.jpg)

I started my part by designing the box to house the cat litter. I used Fusion 360 to make the design parametric, so that it would fit perfectly. I used simple finger joints to connect the different sides. My group mates were the ones that lasercut the files that I sent to them. 

![Image 3: Scooper rake](/images/A13/A13-3.jpg)

![Image 3b: Scooper v0](/images/A13/A13-3b.jpg)

In addition to the box I also drew some test pieces for the rake scooper. The test piece that Anna had 3D printed previously had too small gaps between the spikes, and so it wasn't able to let through the clean sand. As you can see above I drew small pieces of the scooper so that they could be laser cut and tested which would be the best size. 

![Image 4: Scooper sizes](/images/A13/A13-4.jpg)

Above you can see the cut scooper testers. We decided to go with the one with 5mm gaps. 

![Image 5: Scooper test](/images/A13/A13-5.jpg)

After this my group mates drew and cut a bigger size scooper and attached that to a 8mm smooth rod, for further testing. During this period of time I wasn't able to contribute anything to the project remotely, as the process involved a lot of trial and error with the physical parts. My groupmates did a lot of work on the machine and made a lot progress with the other parts and got it moving. You can see more of that process on the machine gitlab page or on their web pages. 

![Image 5b: Acrylic scooper test](/images/A13/A13-5b.jpg)

When I was able to come in to the lab, I picked up work on the project again. I was focusing on the scoop and getting that working. The tester scoop the others had been using was cut from plywood, but the idea was to use acrylic for the final piece, and use heat to bend the scoop to make it more three dimensional, and help it better pick up the poop. 

![Image 5c: Bender 1](/images/A13/A13-5c.jpg)

![Image 5d: Bender 2](/images/A13/A13-5d.jpg)

After cutting a small piece of the scooper from 4mm acrylic I used the machine in the lab to heat up the acrylic on the right spot, and pressed it down to bend it to the correct angle. 

![Image 6: Rod grabbers](/images/A13/A13-6.jpg)

![Image 6b: Rod grabber model](/images/A13/A13-6b.jpg)

The next issue I focused on was the connection between the rod and the scooper. Because there was little to no friction even with small amount of force the rake was able to quite freely rotate around. That would obviously be bad in an actual usage situation. I tried to tighten the metal grips that we were using, but i noticed they were very rigid, and it felt like they wouldn't tighten much more, if any. I thought it would be a good idea to add some silicone tape in between the rod and the clip, to introduce friction. The problem was the hole in the metal clip was too tight to have anything in between the rod and the clip. So I decided to try and 3D print a clip to hold the scooper, which you can see above. 

![Image 7: Rod grabbers & tape](/images/A13/A13-7.jpg)

![Image 8: Tape](/images/A13/A13b-04.jpg)

The part seemed to work exactly as I had hoped. I was able to put some of the repair tape that I had left over from a previous project under the clip, and tighten it enough to hold the scooper firmly in place. After connecting the scooper to the clip I was just barely able to rotate the scooper, but the force needed to do that should be much greater than needed, unless the machine was truly jammed. 

![Image 9: Break 1](/images/A13/A13-9.jpg)

There was a small break in the clip as I was tightening it. I decided to change the orientation of the print, as 3D prints are generally stronger across layers than along layers. 

![Image 10: 3 clips](/images/A13/A13-10.jpg)

Otherwise I was happy with the clip design, so I printed all 3 of the clips. 

![Image 11: 3 Break 2](/images/A13/A13-11.jpg)

Unfortunately I didn't realise that much more durability was needed along the actual clip that holds the rod, and with the different orientation of the layers that was able to break easily. 

One issue could also be the quality of the print, which wasn't so great on the machine in Fablab, so I decided to print the parts at home with my own machine. 

![Image 12: Scooper](/images/A13/A13-12.jpg)

Next I cut a full sized scooper from 4mm acrylic, and attached it to the rod. 

## Design Files

* [Box F3D](/DesignFiles/A13/PoopScooperBoxV1.f3d)
* [Scoop DXF](/DesignFiles/A13/Scooper_v2.dxf)
* [RodGrabber STL](/DesignFiles/A13/RodGrapper_v3.stl)