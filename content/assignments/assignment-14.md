+++
title = "Week 13 : Input devices"
+++

## Week 13 : Input devices

This week our focus was on input devices, and the assignment requirements are as follows:

* Design a microcontroller board with a sensor of your choice. Make sure the board has a working FTDI connection.
* Program your board to read values from the sensor and send them to your computer.
* Include a hero shot and source files of your board in your documentation.
* Describe what you learned about the sensor that you are using on your documentation website.

At the beginning of this week I didn't really know what I should do with this week's assignment, and what sensor I should use. My final project will have at least two types of sensors, a temperature sensor and a pH sensor. Therefore ideally I was thinking I would like to make a microcontroller board that could facilitate both of those sensors. However even though I've learned so much about electronics during this course, I'm still not confident enough designing a board completely from scratch. I remembered Neil mentioning a project called [Adrianino](https://fabacademy.org/2020/labs/leon/students/adrian-torres/adrianino.html), which was made by a former Fab Academy student. It is a modular board using ATtiny 1614 (or 1624) chip, that could be used with multiple different sensors and actuators without having to make a new board for each of them. This sounded like exactly what I wanted, so I decided to build the board, and try to use it to test out a couple different sensors. 

Adrian has an amazing documentation included with the Adrianino, which made building it a breeze! I looked at his images of the schematics for his board to include all the same components in mine. 

![Image 1: Adrianino schematics](/images/A14/A14-1.jpg)

![Image 2: Adrianino PCB](/images/A14/A14-2.jpg)

In the schematics I followed Adrian's board quite religiouslu, but with the PCB I wasn't able to connect everything on one side, so I added in some traces on the back just to be safe and make sure everything was working properly. 

![Image 3: Milled board](/images/A14/A14-3.jpg)

![Image 4: Soldered board](/images/A14/A14-4.jpg)

After milling the board looked great, so I took it out and placed the rivets through the drilled holes. I collected all the necessary parts and soldered everything together. Unfortunately at the time of first writing this documentation, I haven't been able to see whether or not the board works. 

![Image 5: Arduino error](/images/A14/A14-5.jpg)

I borrowed a UPDI programmer from the lab and tried to program the board, but unfortunately kept getting the above error message when trying to upload the code. I made sure multiple times that all the settings should be correct, I had the correct board selected, the right port selected, and so on. I kept getting the same error with two other boards, one of which I was sure that was working, so I figure the issue must be either with the program/my settings, my computer/USB-ports, or the programmer. 

![Image 6: Arduino uno](/images/A14/A14-6.jpg)

At this point the week was already so far along I didn't have much time left until the next lecture where we would be going through our assignments, so I decided to use an Arduino Uno to get at least a little bit of programming in this week. I know it doesn't fulfill the assignment requirements, as we need to use a board we've designed, but I will get the Adrianino working later on and update it on this page. 

![Image 7: LEDs and potentiometer](/images/A14/A14-7.jpg)

I decided to use an LED strip and a potentiometer I had at home to test out the programming on the Arduino Uno. Not the most exciting project I know, but would have to do for now. 

![Image 8: LEDs test](/images/A14/A14-8.jpg)

The LED strip I had was WS2812B, and is compatible with [FastLED library](https://fastled.io/). I followed their [get started](https://github.com/FastLED/FastLED/wiki/Basic-usage) documentation to test out how the strip works. 

``` bash
    #include <FastLED.h>
    #define NUM_LEDS 14
    #define DATA_PIN 9
    CRGB leds[NUM_LEDS];

void setup() {
  // put your setup code here, to run once:
    FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);

}

void loop() {
  // put your main code here, to run repeatedly:
leds[0] = CRGB::Green; 
        FastLED.show(); 
        delay(30); 
}
``` 

With the code above I was able to turn on the first LED with green light. There's so much more that can be done with the lights, but this week is all about the inputs so next I wanted to focus on the potentiometer. 

![Image 9: Potentiometer](/images/A14/A14-9.jpg)

For the potentiometer I followed [this](https://www.instructables.com/How-to-use-Potentiometer-Arduino-Tutorial/) tutorial to get it set up. Below you can see the initial code I used. 

``` bash
const int potPin = A1; //pin A1 to read analog input

//Variables:
int value; 

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(potPin, INPUT);  

}

void loop() {
  // put your main code here, to run repeatedly:
  value = analogRead(potPin);
  Serial.println(value);
  delay(100);
}
``` 

This was able to give me the different values when I was turning the knob of the potentiometer.

{{< video src="/images/A14/A14-V1.webm" type="video/webm" preload="auto" >}} 

Finally it was time to put the two objects together, and control the LED lightstrip with the potentiometer. At the bottom of the page of the FastLED documentation there is instructions on how to use a potentiometer to control the number of LEDs that are turned on, which sounded like fun. So I followed the instructions and ended up with the following code:

``` bash
    #include <FastLED.h>
    #define NUM_LEDS 15
    #define DATA_PIN 9
    #define potPin A1
    CRGB leds[NUM_LEDS];

void setup() {
  // put your setup code here, to run once:
    FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);

}

void loop() {
  // put your main code here, to run repeatedly:
    int val = analogRead(potPin);
    int numLedsToLight = map(val, 0, 1010, 0, NUM_LEDS);
    
  // clear existing led values
  FastLED.clear();
  for(int led = 0; led < numLedsToLight; led++) {
    leds[led] = CRGB::Blue;
  }
  FastLED.show();
}
``` 

{{< video src="/images/A14/A14-V2.webm" type="video/webm" preload="auto" >}} 

Above you can see the video of the code working. 

## Getting Adrianino working

![Image 10: Adrianino](/images/A14/A14-10.jpg)

It took me a whole day working on it, but 