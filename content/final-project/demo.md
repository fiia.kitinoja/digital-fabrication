+++
title = "Final Project"
Description = "Demo video"
+++

## Aquaponics system and water monitoring app

![Image 4](/images/FinalProject/FPV-5.jpg)

{{< video src="/images/FinalProject/FiiaKitinoja-DFVideo.webm" type="video/webm" preload="auto" width="800px" >}}

![Image 1](/images/FinalProject/FPV-1.jpg)

![Image 2](/images/FinalProject/FPV-2.jpg)

![Image 3](/images/FinalProject/FPV-3.jpg)

![Image 4](/images/FinalProject/FPV-4.jpg)
