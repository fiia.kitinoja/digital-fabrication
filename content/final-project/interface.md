+++
title = "The body"
Description = "The body of the aquaponics system"
+++

During the project I have gone a bit back and forth on how I wanted my monitoring interface to look like, and whether it would be on an integrated screen on the structure, or on the web. Ultimately I decided to go with a web interface, for maximised customiseability.

During the process of working on the web application and the Raspberry Pi I got a lot of help from my boyfriend, who works as a developer and basically does this stuff for a living. He explained the whole process to me and guided me through each of the steps. With his help I understand the code I have written, however backend coding is still very complicated to me and I don't think I would be able to do the whole process again completely on my own just yet. However it'll definitely be a lot easier for me to carry on learning.

Main technologies used:

| **Name**                      | Location     | Purpose                                                                                                                                                                                                                                                                                                                                                     |
| ----------------------------- | ------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **React**                     | Raspberry Pi | React will serve as the frontend. It allows dynamic content to be updated more easily than regular HTML. It also has many supporting libraries and a vibrant open source ecosystem.                                                                                                                                                                         |
| **Express**                   | Raspberry Pi | ExpressJS is a server side JavaScript (also known as NodeJS) library, it will serve as the project backend or web server. Here we will create the API that our frontend will consume to display the interface.                                                                                                                                              |
| **RethinkDB**                 | Raspberry Pi | RethinkDB is a document database with excellent support for real-time data. Other document databases lack this support hence it was chosen for this project, as a dashboard is inherently real time. It is installed on the system of the raspberry pi and used by the express server to fetch data. It also has first party support for NodeJS and Python. |
| **Python Serial Interceptor** | Raspberry Pi | This Python script will be listening on the serial connection to the raspberry pi from the microcontroller. The script also feeds data to the RethinkDB instance.                                                                                                                                                                                           |
| **SocketIO**                  | Raspberry Pi | This is a small library that will be running alongside the express server. A complimentary library will be installed alongside react to facilitate the real time data connection using web sockets.                                                                                                                                                         |

![Int 0: Architecture](/images/INT/architecture.png)

## React

#### Setting up

Setting up react was quite beginner friendly. I also wanted to use [Tailwind](https://tailwindcss.com/) to help build the front end of my application, so I followed their [getting started with React](https://tailwindcss.com/docs/guides/create-react-app)
guide.

I followed the instructions of [ReactJS](https://reactjs.org/docs/create-a-new-react-app.html)

```bash

npx create-react-app aquamonitor
cd aquamonitor
npm start

```

Next I wanted to install Tailwind CSS, so I went on to their [website](https://tailwindcss.com/docs/installation).

```bash

npm install -D tailwindcss postcss autoprefixer
npx tailwindcss init -p

```

After that I was ready to play with the design of the interface. My first aim was to use Tailwind CSS to create a grid, and then add boxes to the grid that would have the different values, and graphs for them.

I deleted the code on the App.js file that I didn't need, and created basic boxes aligned in a flexbox grid from Tailwind.

![Int 1: Boxes](/images/F17/F17-1.jpg)

```bash

function App() {
  const [roomTemp, setRoomTemp] = useState([]);
  const [waterTemp, setWaterTemp] = useState([]);
  const [pH, setpH] = useState([]);
  const [waterLevel, setWaterLevel] = useState([]);

  return (
    <div className="App bg-blue-400 h-full">
      <nav></nav>
      <main className='container mx-auto px-10'>
        <div className='flex justify-center flex-row-reverse gap-4'>
          <Row>
            <InfoCircle data={roomTemp} icon={faTemperatureHalf} ></InfoCircle>
          </Row>
          <Row>
            <InfoCircle data={waterTemp} icon={faDroplet} ></InfoCircle>
          </Row>
          <Row>
            <InfoCircle data={pH} icon={faVial} ></InfoCircle>
          </Row>
          <Row>
            <InfoCircle data={waterLevel} icon={faWater} ></InfoCircle>
          </Row>
        </div>
        <Box>
          <div className="flex flex-row items-center align-center ">
            <div className='flex flex-col items-center'>
              <SimpleLineGraph data={roomTemp} title='Room Temperature'></SimpleLineGraph>
            </div>
            <LineGraph data={waterLevel} title='Water Level'></LineGraph>
            <LineGraph data={pH} title='pH'></LineGraph>
            <LineGraph data={waterTemp} title='Water Temperature'></LineGraph>
          </div>
        </Box>
      </main>
    </div>
  );
}

```

#### React and Components

React is a 'reactive' javascript library allowing data to be updated across many places in a website when it is changed once. It heavily features code reuseability in the form of components. The main idea is we will have one 'main' component, whose job it is to retrieve our data (this is App.js), and several child components that will display the data in different ways.

Components are reuseable sections of code that contains both UI elements and data. I made a component called 'Infocircle' that house the most up to date sensor value, which include water level, pH, water temperature, and room temperature. I have plans to add a good bit of information on the application, including some forms for manual input and so on, but for the purposes of the digital fabrication course I will only focus on displaying the sensor information, and maybe having controls for the lights, depending on how difficult it will be to achieve two-way communication between the raspberry pi and my board.

The other components I used include graph libraries to display data over time using different graphing techniques. Something to add in the future would be custom graph components, such as an animated water level illustration.

#### Data Fetching / API integration

Next I will go through little bit about my client side API setup. For this I am using [Axios](https://github.com/axios/axios), which is a lightweight HTTP client. [This](https://www.digitalocean.com/community/tutorials/react-axios-react) tutorial goes through the installation and the capabilities of axios quite well.

After installing axios I created a new folder inside src/api. In that new folder I made two new files.

**api.js**

```bash

import axios from 'axios'

export default axios.create({
    baseURL: process.env.REACT_APP_API
})

```

In the first file I imported axios so that they can be used in the component. The function below the import creates a new axios instance using axios.create function. I configured the baseURL to link to a dotenv file where I have specified the URL. More on dotenv later.

**sensor.js**

```bash

import api from './api'

async function getRoomTemp() {
    return api.get('/sensor/room_temp')
}

async function getWaterTemp() {
    return api.get('/sensor/water_temp')
}

async function getpH() {
    return api.get('/sensor/pH')
}

async function getWaterLevel() {
    return api.get('/sensor/water_level')
}

export { getRoomTemp, getWaterTemp, getpH, getWaterLevel, }

```

The second file I created, called sensor.js, includes the GET requests for each of the sensors. I first imported the api.js file and then created asynchronous functions for each of the sensors, which include the routes to the backend API, and then exported them. These functions are named after the respective sensors which data they are fetching, such as getRoomTemp, getpH, etc. With these functions exported I can now call them inside the App.js file. We'll take advantage of React's data storage by using _React hooks_.

```bash

import { useState } from 'react';

function Example() {
  const [count, setCount] = useState(0);

  return (
    <div>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>
        Click me
      </button>
    </div>
  );
}

```

Above is a functional example for the `useState` hook from the React documentation. `useState` will return an array with two variables inside. The first one will be a variable referring to the piece of state we have initialised. The second is a function we can use to update the state. We can pass a parameter to `useState` to initialise our default.
State in react is immutable, this function provides us a way to update state in react safely. The intricasies of internal state management are outside of scope for this documentation.
In the above example we use `setCount`, which is the second variable returned from `useState` to update the value of count.
Note, we use the current value of count to update the count.
Because of the "reactivity" in react, this data will always be kept up to date in our child components when we change it in the App.js.

The other hook we use here is `useEffect`. To understand what `useEffect` does, you need to know a little bit about how react works.
Once a page is loaded, the entire component(function) is run, if any state inside the component is ever updated, the entire component will need to be rerendered, ie. the entire function is run again.
`useState` is smart enough to only use its initial value on the first render and use any updated value on the subsequent rerenders. Thus avoiding an infinite loop of component rendering, the data being updated (counter being incremented) causing component to rerender causing update data and so on.
However sometimes we do only want to run things once, and ignore them on subsequent rerenders. This is where `useEffect` comes in. This built-in react hook allows us to run code on the first render of the component, and ignore any subsequent rerenders.

```javascript
useEffect(() => {
  fetchInitialData();
}, []);

const fetchInitialData = async () => {
  const roomTempData = await getRoomTemp();
  const pHData = await getpH();
  const waterTempData = await getWaterTemp();
  const waterLevelData = await getWaterLevel();
  setRoomTemp(roomTempData.data);
  setpH(pHData.data);
  setWaterTemp(waterTempData.data);
  setWaterLevel(waterLevelData.data);
};
```

#### Changing data / Realtime

// TODO
Touch on client side socketio (this will be talking about pushNewData & setupRealtimeData, dont worry about how the data is getting here for now, it will be explained later, for the reader)

## Express

As mentioned in the table above Express is the backend for the application. The backend acts as an intermediary between your frontend and the database. In production, this can alleviate security concerns, as clients can be made malicious. Defining our data API in the backend allows us to only define what data we want the client to see.
In my specific use case, which is self-hosted, this isn't a concern, but modern browsers will prevent access to databases directly. So best practices are still enforced.

I followed [this](https://www.mongodb.com/languages/mern-stack-tutorial) to set it up, with the change of instead of MongoDB I used RethinkDB database. The 'MERN' stack is a common beginner full-stack framework in the web development industry. The change to RethinkDB is used to better facilitate real time data.

#### Setup

// TODO
go through mern stack tutorial, putting in very basics (initial app.js file, note mongoDB not being used here)

#### RethinkDB Dabase

// TODO
To replace Mongo, we used RethinkDB, for real time support.
The files are db/conn.js for the initial connection/
I created a utility library file called db/db.js that houses all database functions. (it abstracts the database functions and simplifies it to one function.) The database functions here are createTable, insert, getAll, getSensorData & realtimeSensorData.

#### API

Once you finish the above setup, you will have an API for books and authors (through /routes/books.js tjsp)
First we did was setup a new route file inside routes folder called sensors.js. In here we made our API for our sensor data.
Then we call the abstracted functions.
Touch on what a REST api is (google)
Touch on what endpoints are, and what HTTP verbs (get, post, etc.) are in relation to your endpoints.

## RethinkDB

RethinkDB has a coloured past, starting out as a failed startup in 2015, they were bankrupt in 2017 and the project died. Some employees from Rethink were passionate enough about the project to try to resurrect it as an open source venture. However the liquidators involved in the bankruptcy wanted to sell the source code assets, which the developers could not afford. The Linux Foundation stepped in to purchase the rights and RethinkDB has a new lease on life under LF, fully open sourced and under active development again.

I installed RethinkDB locally with docker, which made it pretty easy. After that I followed [this](https://rethinkdb.com/docs/guide/javascript/) guide to interface with the backend. [See Express section RethinkDB](#rethinkdb-database)

On the Raspberry Pi you cannot use docker, due to the CPU architecture (ARM), so you have to install the program from source. This can take upwards of four hours, and failed once. Afterwards you're given a single binary, which can be run in rc.local to start the database in startup.

## Python script

// TODO
I followed a youtube video from (write about it here.)
The basics are it uses a library called pyserial to listen to the serial port on the Pi. We then use the first party python library from [RethinkDB](https://rethinkdb.com/docs/guide/python/) to send the data to the database.
touch on data sending method. Serialised json, ie. a string that is also json. This is then deserialised in the python script.

// TODO

## SocketIO
