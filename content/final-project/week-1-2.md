+++
title = "Final Project"
Description = "Initial sketch"
+++

## Final Project: Indoor Aquaponic System

For my final project I would like to make an aquaponic system for my apartment. I have been interested in the concept of aquaponics for a while, and have wanted to try it myself. I thought that the digital fabrication course would be a great opportunity to bring that project into fruition. We have had a fish tank before, so I have some experience in keeping fish, but I am interested in developing that into (somewhat) self-sustaining ecosystem.

### Specs

To start I did a lot of research to see what type of setups other people have created. I found two other aquaponics Fab Academy projects that have been and will be a great help during this projects. Those projects can be found [here](https://fabacademy.org/2021/labs/charlotte/students/theodore-warner/Final%20Project/final-project/) and [here](http://archive.fabacademy.org/archives/2016/greenfablab/students/365/project03.html).

After looking at the examples I wanted to write down any specific ideas I wanted to be included in my project.

- Tank around 100 L
- Ornamental fish
- Lights for plants
- Tank accessible easily for maintenance if needed
- Automated lights & sensors

After some sketching I came to the conclusion of the tank size of 800 x 400 x 400 mm. Rest of the measurements are following from that. Below is a v1 sketch, and a screenshot of a work-in-progress 3D-model.

<img src="/1.jpg" alt="sketch version 1" width="600">

<img src="/2.PNG" alt="3d model" width="600">

## Bill of Materials

####

 <table>
  <tr>
    <th>Material</th>
    <th>Quantity</th>
    <th>Price per item</th>
    <th>Total price</th>
    <th>Sourced</th>
  </tr>
  <tr>
    <td>Alfreds Futterkiste</td>
    <td>Maria Anders</td>
    <td>Germany</td>
  </tr>
  <tr>
    <td>Centro comercial Moctezuma</td>
    <td>Francisco Chang</td>
    <td>Mexico</td>
  </tr>
</table>
