+++
title = "Raspberry pi"
Description = "Raspberry pi"
+++

![Image 11: Raspberry pi](/images/F14/F14-11.jpg)

There were some unused Raspberry pis in the fablab, so I decided to use one of them as a webserver, and have my interface be available through a website, since I was planning on using a raspberry pi camera in my setup anyway. Kris instructed me to get it set up, by flashing the [Operating System](https://www.raspberrypi.com/software/operating-systems/) on the SD card. I used Raspberry Pi Imager for that.

![Image 12: Raspberry pi](/images/F14/F14-12.jpg)

After doing that I connected a monitor, keyboard and a mouse to the pi, and saw it was working properly. I set the locale to Finland as the terminal was instructing me to do.

## Creating a web server

##### Step 1: Log in to pi through SSH

Since the pi and the operating system I'm using on it don't have a graphical interface, I'm using it through a terminal. I can use it by connecting the pi into a screen, keyboard and a mouse, but then I would need to have essentially two computers in front of me, and while it is easier to do at the lab, I don't have the space for it at home. The other option is to SSH into the pi from my own laptop, while it is powered on and connected to the internet.

![Image 1: IP address](/images/F14/F14-1.jpg)

First we need to figure out the IP address of the pi. I plug in the power and an ethernet cable to make sure it is connected to the internet, and then open a website to my router at home, which in my case is 192.168.0.1. From there I click on clients, and then will see a list of devices connected to my home network. I find the pi from the list, and I can see that the IP address is 192.168.0.109.

Next I open my terminal and type in the command to log in.

```bash

ssh username@IPaddress

ssh raspberry@192.168.0.109

```

![Image 2: ssh fail](/images/F14/F14-2.jpg)

As you can see above, the connection attempt was refused. It could be because the username or ip address was wrong, but also because the SSH might not be enabled. I found [this](https://howchoo.com/g/ote0ywmzywj/how-to-enable-ssh-on-raspbian-without-a-screen) tutorial on how to enable SSH without a screen, which I followed.

##### Step 2: Enable SSH

Enabling ssh seems to be fairly easy. I powered off the pi, removed the SD card and inserted it into my laptop.

```bash

cd /Volumes/boot
touch ssh

```

I then moved into the boot directory through my terminal, and created a file called ssh. After that all I had to do is to remove the SD card, re-insert it into the pi and then power it back on while being connected to the internet.

![Image 3: ssh success](/images/F14/F14-3.jpg)

I try the SSH command again, and this time I'm able to put in my password after some warnings about the trustworthiness of the host.

##### Step 3: Installing Nginx

I'm planning on having my interface with all the settings and sensor information to be on a web application, because I think that way I can include more features than I could, if I just had a screen connected to the aquaponics system itself. I'd like to have graphs showing the changes over time of the sensor input, as well as have the ability to log in information of the seeding and harvest time (etc.) of the plants that I'm growing. I probably won't be able to finish everything I want on the interface by the end of the semester, but it's something I'll work on even afterwards.

Anyway, to make that happen, I need to create a web server on the pi. [Nginx](https://www.nginx.com/) is one of the most popular web servers in the world, so I decided to go with that one. I am following [this](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-20-04) tutorial from Digital Ocean.

Nginx is available in Ubuntu's default repositories, it can be installed using the apt packaging system, as long as the system is up to date.

```bash

sudo apt update

```

Next we will run the command to install Nginx.

```bash

sudo apt install nginx

```

##### Step 4: Checking the web server

The tutorial is recommending to adjust the Firewall settings after installing Nginx and before checking the web server. However I'm planning on having the site only be accessed within my home network, so I won't be needing a firewall, so I skipped to the next section.

The server should be up and running after the installation, which we can check with the systemd init system.

```bash

systemctl status nginx

```

![Image 4: server check](/images/F14/F14-4.jpg)

Now we have confirmation that the server has started successfully. Another way to test this is by attempting to visit the site, for which we will use the pi's Ip address. So I opened up a new tab on my browser, and typed in http://192.168.0.109.

![Image 5: server check 2](/images/F14/F14-5.jpg)

##### Step 5: Managing the Nginx process

Now that we have the server up and running, the tutorial runs through a few basic management commands, which I will document here for the future.

Stopping the webserver:

```bash

sudo systemctl stop nginx

```

Starting the web server:

```bash

sudo systemctl start nginx

```

Restarting the web server:

```bash

sudo systemctl restart nginx

```

Reload the web server (for making configuration changes nginx can be reloaded without dropping connections):

```bash

sudo systemctl reload nginx

```

When the server boots, Nginx will start automatically. Disabling automatic start:

```bash

sudo systemctl disable nginx

```

Re-enabling automatic start:

```bash

sudo systemctl enable nginx

```

##### Step 6: Setting up server blocks

Next the tutorial goes through setting up server blocks. This is so you could have more than one domain from the one single server.

![Image 6: Nginx directory structure](/images/F14/F14-6.jpg)

The websites live in the var/www/html/ folder, and by navigating there we can see the html code for the default web page we saw earlier.

```bash

cd /var/www/html
less index.nginx-debian.html

```

![Image 7: default html](/images/F14/F14-7.jpg)

Next I will create directory for my domain and assign ownership with the $USER environment variable.

```bash

sudo mkdir -p /var/www/aquaponics/html
sudo chown -R $USER:$USER /var/www/aquaponics/html

```

Next I checked that the permissions are correct and that the owner can read, write, and execute the files I input the following command:

```bash

sudo chmod -R 755 /var/www/aquaponics

```

Then I create a sample index.html page.

```bash

sudo nano /var/www/aquaponics/html/index.html

```

![Image 8: nano](/images/F14/F14-8.jpg)

The command open up our newly created file in nano in the terminal window, and I inserted in the sample code from the tutorial. I saved and closed nano by pressing Control + x, then type Y and press Enter.

Now that I have a new domain, I need to create a server block so that the server can actually serve the content. The server blocks live inside the /etc/nginx/sites-available directory, which is essentially the folder for all the domains we're working on. However there is another directory within /etc/nginx folder, called sites-enabled. This folder will have symbolic links, which link to the files inside sites-available, which, funnily enouogh, won't be available to access until being enabled, through creating symlinks to sites-enabled.

```bash

sudo nano /etc/nginx/sites-available/aquaponics

```

![Image 9: nano2](/images/F14/F14-9.jpg)

After creating a new server block in sites-available I inserted the configuration block from the tutorial, substituting your_domain with aquaponics.

Next I created the symlink in sites-enabled. Then I adjust a value in /etc/nginx/nginx.conf file to avoid possible hash bucket memory problems that could arise from additional server names.

```bash

sudo ln -s /etc/nginx/sites-available/aquaponics /etc/nginx/sites-enabled
sudo nano /etc/nginx/nginx.config

```

I searched for the server_names_hash_bucket_size line with control + W and uncommented the line, then saved and closed.

Next I tested if there are any syntax errors in any of my nginx files.

```bash

sudo nginx -t

nginx: [emerg] open() "/etc/nginx/sites-enabled/aquaponics" failed (2: No such file or directory) in /etc/nginx/nginx.conf:60
nginx: configuration file /etc/nginx/nginx.conf test failed

sudo systemctl restart nginx

Job for nginx.service failed because the control process exited with error code.
See "systemctl status nginx.service" and "journalctl -xe" for details.

```

I see that it doesn't find the symlink to my domain file, so I ran the code to restart the server and enable changes. However I got another error message.

![Image 10: broken symlink](/images/F14/F14-10.jpg)

I navigated over to sites-enabled folder and I see that the aquaponics link that I created is broken as it is red. I tried to create the link again, however I got an error saying the link already exists. With the help of Google I found [this](https://stackoverflow.com/questions/52539640/symbolic-link-broken-after-creation) post, which sounded like the exact same issue I was having. I first removed the previous link, then navigated to the sites-enabled directory and created the symlink again. After those steps the link was showing blue, meaning it should be working. The reason I had to do it differently from the tutorial could be that I was moving in the directories, which I don't think they did in the tutorial.

```bash

cd /etc/nginx/sites-enabled
sudo rm -r aquaponics
sudo ln -s /etc/nginx/sites-available/aquaponics ./

sudo ngingx -t
nginx: [emerg] unknown directive "/etc/nginx/sites-available/aquaponics" in /etc/nginx/sites-enabled/aquaponics:3
nginx: configuration file /etc/nginx/nginx.conf test failed
```

I still got an error message, however a different one this time. Based on [this](https://stackoverflow.com/questions/19165976/nginx-emerg-unknown-directive-in-etc-nginx-sites-enabled-example-com3) thread the error most likely had something to do with badly copy pasted code. I removed both aquaponics files in sites-enabled and sites-available, and opened the index.html file again in nano. I emptied the file and copied the sample html again from the tutorial, this time making sure I copied it by clicking the button on the corner of the textbox instead of selecting the text with my cursor I followed the steps again from thereon, and after creating the symlink again and checking the configuration file I got no errors.

## Getting started with NextJS

I have decided to use React to build my web application, mostly because I don't know much about libraries and it was recommended to me by my partner so I thought why not. [NextJS](https://nextjs.org/) is a framework for React, and again I was told it would be good for my project so that's what I'm going with! To learm more about using it, I will be following their [get started](https://nextjs.org/learn/foundations/about-nextjs?utm_source=next-site&utm_medium=homepage-cta&utm_campaign=next-website) lessons, which I won't be documenting here, until I actually get into building my web app for the aquaponics system.

## React app

I followed the instructions of [ReactJS](https://reactjs.org/docs/create-a-new-react-app.html)

```bash

npx create-react-app aquamonitor
cd aquamonitor
npm start

```

Then I pushed the directory to gitlab to have version control.

```bash



```

Next I wanted to install Tailwind CSS, so I went on to their [website](https://tailwindcss.com/docs/installation).

```bash

npm install -D tailwindcss postcss autoprefixer
npx tailwindcss init -p

```

Added the paths to my template files.

```bash

module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}

```

Finally added Tailwind directives to the react css files, and then started the build process.

```bash

@tailwind base;
@tailwind components;
@tailwind utilities;

npm run start

```

After that I was ready to play with the design of the interface. My first aim was to use Tailwind CSS to create a grid, and then add boxes to the grid that would have the different values, and graphs for them.

I deleted the code on the App.js file that I didn't need, and created basic boxes aligned in a flexbox grid from Tailwind.

```bash

// import logo from './logo.svg';
import './App.css';

function Box({ children }) {
  return (
    <div className='bg-blue-100 rounded py-6'>
      Box
    </div>
  )
}

function Grid({ children }) {
  return (
    <div className='grid grid-cols-5 gap-2'>
      {children}
    </div>
  )
}

function App() {
  return (
    <div className="App">
      <nav></nav>
      <main className='container mx-auto'>
        <Grid>
          <Box></Box>
          <Box></Box>
          <Box></Box>
          <Box></Box>
          <Box></Box>
          <Box></Box>
          <Box></Box>
          <Box></Box>
          <Box></Box>
          <Box></Box>
          <Box></Box>
        </Grid>
      </main>
    </div>
  );
}

export default App;

```

![Image 11: box grid](/images/F14/F14-11.jpg)

Next I wanted to try look at some charts for my data. I chose [react-vis](https://uber.github.io/react-vis/) library and installed it.

```bash

npm install react-vis

```
