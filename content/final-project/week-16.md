+++
title = "The body"
Description = "The body of the aquaponics system"
+++

Building the actual body and structure of the aquaponics system is perhaps the most time- and definitely the most material-consuming part of the project. I was kicking the can down the road for way too long, as I knew once I started building it there was no more going back and changing it. I finally kickstarted the process in early May when I realised I was quickly running out of time.

There are almost as many different looking aquaponics system as there are the hobbyists. There are some commercial units available to be purchased, but from my research it is a lot more common for people to build their own.

The scale of the system is perhaps one of the deciding factors guiding the design process. The scale can vary between huge industrial, warehouse-wide systems to a small, desktop 20-liter ornamental aquarium with a small growbed on top.

![Image 1: Aquaponics systems](/images/F16/F16-1.jpg)

Scale-wise my project lands somewhere in between, while still leaning heavily on the smaller scale. For an indoor, ornamental system it is definitely on the larger side, however overall it is still considered very much a small scale system, as the most common aquaponics systems are backyard systems that grow the fish to be eaten as well, Tilapia being one of the most common species due to its fast growth rate.

## V1 Sketch

![Image 2: Initial sketch](/images/F16/F16-2.jpg)

![Image 3: Initial 3D model](/images/F16/F16-3.jpg)

From the start I knew I wanted a fairly sizeable aquarium for a few reasons. Firstly I knew the whole project would be a lot of work regardless, and I wanted to be able to grow more than just a couple plants to make all of it worth it. Secondly a bigger tank size would give me a lot more flexibility in terms of fish species without having to overstock it. The size I had in my head for the aquarium was 80 x 40 x 40 cm, which equates to about 128l. I was planning on making the aquarium myself using acrylic sheets, which would make it easier to get the exact size I wanted. From everything I read online the size of the growbed should be the same size as the aquarium in terms of litres, so that there would be enough medium for the bacteria to grow on. I wanted to maximise the amount of planting area, so I was planning on making the growbed roughly to the dimensions of 110 x 40 x 30 cm, which is the reason it is longer in the initial sketch & model.

In my initial plan I wanted to have the aquarium be separate from the growbed, because I wanted it to be easily accessible if needed, for maintenance etc. I was planning on achieving this by having the outer frame holding the growbed and the aquarium being on a separate table underneath it, on wheels, so that when I needed to work on the aquarium I could just wheel it out from under the growbed. I didn't realise what a bad idea this was, until just before I actuallt started building the system. You should never move an aquarium full of water, as the water moving around will distribute the weight unevenly, and it could damage the seals. After realising this in order to achieve what I wanted I knew it had to be the growbed that would have to be easily removed from the frame and have the aquarium remain stationary.

## Sourcing the aquarium

After doing some thinking I also decided instead of making the aquarium myself I would buy it instead. It would be much more expensive, but I wouldn't have to worry as much about a hundred liters of water suddenly flooding the apartment. Or if it did at least I would have some recourse. However I would like to try my hand at making an aquarium from scratch, and [Teddy Warner](https://fabacademy.org/2021/labs/charlotte/students/theodore-warner/Final%20Project/final-project/) did just that with his aquaponics system, which I would like to follow, but with a lot smaller aquarium to start with.

![Image 4: Tank](/images/F16/F16-4.jpg)

Anyway, I visited a local aquarium shop and purchased a glass aquarium with the dimensions of 100 x 40 x 40 cm. They had just sold their last 80 x 40 x 40 cm aquarium, and I figured the increase from 128l aquarium to a 160l aquarium wasn't that big of a jump in the project scale. Now with the main item purchased for the system, I could start designing the rest of the components around the aquarium.

## Small scale model

I was trying to figure out how to make the growbed separable from the aquarium, while still having a stable, rigid structure. My first idea was to just put the wheels under the frame instead of the aquarium, but that didn't feel like a very practical solution. I decided to try build a small scale prototype to help me visualise the project better.

Another issue I needed to solve in addition to the removability of the growbed was the material. With a 160l aquarium the weight of the aquarium itself would be roughly 200kg, in addition to the growbox and the medium above it, and it all needed to be strong and stable. I'm sure it would be possible to build such a structure out of wood, however you would have to have a lot of knowledge about woodworking. Metal structure would be a lot more forgiving of mistakes during the building process, as it has the capacity to hold much bigger loads.

![Image 5: Model 1](/images/F16/F16-5.jpg)

![Image 6: Model 2](/images/F16/F16-6.jpg)

![Image 7: Model 3](/images/F16/F16-7.jpg)

![Image 8: Model 4](/images/F16/F16-8.jpg)

![Image 9: Model 5](/images/F16/F16-9.jpg)

With the dimensions of the aquarium firmly locked in place it was easy to start building the model. I purchased a pack of paper straws to work as the metal bars, and used some packaging cardboard for the rest. I built the model in scale of 1:10, starting with the aquarium. I ended up with the model shown above, with overall size of roughly 100 x 40 x 180 cm.

Once I had the model finished and a good idea of the body of the system I had to figure out how to build it. Luckily in Aalto university we have amazing resources when it comes to making practically anything. As an arts-student I had access to the metal workshop, and so I went to the workshop masters with my model, to talk to them how to go about building it.

## 3D model

Next step I wanted to make a 3D model in Fusion 360 to lock in the design and have all the measurements for the parts. I had decided to use 25 x 25 mm mild steel box section (huonekaluputki in Finnish). The design also changed somewhat.

![Image 10: 3D model](/images/F16/F16-10.jpg)

With the ned design the growbox would simply be able to be removed by lifting it out of the metal frame, allowing somewhat unobstructed access to the top of the aquarium. I also wanted to include a little stool in to the design, as I realised the top of the growbed would be about 140cm high and it would be somewhat difficult for me to tend to the plants, being only about 160cm tall.

![Image 11: 3D model 2](/images/F16/F16-11.jpg)

![Image 12: 3D model 2B](/images/F16/F16-12.jpg)

After calculating the amount of steel needed for the frame I decided to leave out the non-essential parts, such as the growlight legs and the stool, as I could figure out how to make those from some other, preferably leftover material.

## Construction - Metal Frame

After finishing the 3D model I was able to figure out the lenghts of individual parts I was able to actually start the construction. In this part I would like to remind everyone about the importance of good planning, and please don't do what I did, but what I tell you to do.

Looking back I definitely jumped in to working with the material too soon. The workshop masters in the university mostly work with design students, where it's the course teacher's responsibility to talk over the design of the piece and the possible issues that could arise, and the master's would help the students to make whatever they were looking to to, ti exactly the specifications of the students design. I didn't communicate well enough how my course and project were very different from this, however in the end I was able to tweak my design to make it the best possible. In this section I will go through the steps needed to make the frame in the order you are meant to do them, and how it would work if you were a professional. I am however far from that, so I was jumping between all these steps to fix mistakes etc.

#### Cutting the material

![Image 13: Sketch 2](/images/F16/F16-13.jpg)

Steel Box Section part dimensions

- 4 x 110 cm
- 11 x 35 cm
- 5 x 105 cm
- 4 x 29.5 cm

Total material needed: 14.68 m.

![Image 14: Band saw](/images/F16/F16-14.jpg)

Cutting the correct sized pieces was super easy, as there is a horizontal band saw machine, that does the work for you, and all you need to do is place the material correctly and clamp it down. I tried to source the smallest pieces possible and cut them down to the length I needed to reduce wasted material, instead of using the long pieces.

#### Drilling holes

<!-- Photo of drill -->

Next step was to drill any holes needed to the parts. An advantage of using hollow tubes for the frame instead of solid wood, was that I could hide the wires inside them. For that I needed to know where the wires would have to enter the frame and where they would come out. I used a vertical drill with a 10 mm drill bit. The workshop masters taught me how to operate it, the main important setting being the spinning speed, which depended on the size of the drillbit.

<!-- spinning speed table -->

With a drillbit of 10 mm, in addition to relatively low spinning speed, it was important to do the drilling in short bursts, so that the drill wouldn't become too hot and start burning.

#### Welding

The next two steps are done somewhat concurrently. The structure is divided into modules, which are built by connecting the individual parts through welding. After the modules are finished, the welds will be sanded down using an angle grinder, which should leave a smooth finish. After that the modules will be joined together using the rest of the parts, which then will be angle grinded down again.

Welding was definitely the most intimidating task of the project I have faced so far, but I was surprised how quickly I got used to it. I used small leftover pieces to practice with.

![Image 15: Clamps](/images/F16/F16-15.jpg)

My frame consists of only right angles, which makes it fairly easy to weld together. However it needs to be very stable, so I need to pay very close attention to make sure everything is the correct dimension and angle.

<!-- Photo of welding machine-->

Welding basically uses gas and an extremely hot flame to melt some copper wire into the metal you are working with. By changing the feedrate of the copperwire the temperature will adjust, and usually the thicker material, the higher power and feedrate is needed. The properties of heated metal are different than cool metal, and it will expand/compress slightly based on its temperature. Due to this it is important to use "hefts" (term used in Finnish, not sure whether it is the same in English or what the correct term is) to connect the pieces on all sides first, before welding shut the whole side. They are basically small spots of welds that hold the pieces together.

![Image 17: Hefts](/images/F16/F16-17.jpg)

After connecting all the sides with a small dot of weld, it is time to weld the main holding connection. The sides of the structure will be grinded smooth as I explained earlier, which will leave very little material actually connecting the two parts. Due to this the main load bearing connection will be on the 90 degree corner between the two separate tubes.

![Image 50: Welding motion](/images/F16/F16-50.jpg)

![Image 16: Practice welds](/images/F16/F16-16.jpg)

To make the corner weld as strong as possible I was taught to use the above motion to weld the pieces together, to create the strongest possible connection. In my project it wasn't that important, I could do small dots next to each other as well and the frame should still be able to hold all the weight it needed, but I still wanted to try and learn the best practice. After I was able to somewhat come to grips with the welding process, I moved on to the actual pieces. I build the side modules first by connecting the y and z axis pieces together, and then finally would connect the modules together using the x axis pieces.

![Image 18: Welding 1](/images/F16/F16-18.jpg)

![Image 19: Welding 2](/images/F16/F16-19.jpg)

#### Angle grinding and sanding

After and inbetween the welds I would use a standard angle grinder to smooth out the connections. That process was fairly easy as well. The main things to keep in mind was to not remove too much of the steel, while still grinding enough so that the two pieces looked like one and there was no visible seams. If there were holes I could always add some more weld to it and grind it down again.

![Image 20: Grinding 1](/images/F16/F16-20.jpg)

![Image 21: Grinding 2](/images/F16/F16-21.jpg)

After grinding the side seams I moved to the corners. They wouldn't have as much material removed as the sides, but would still be sanded down to look nicer.

<!-- Photos of angle grinding -->

TBC...

## Construction - Growbox

I was working on the growbox concurrently to the metal frame, as there was a lot of waiting stuff to dry. Whereas building the metal frame was learning a lot of new skills and mostly doing manual work, I was able to use some skills I learned during the Fab Academy to build the growbox, mainly being the CNC machine.

![Image 22: Growbox 3D model](/images/F16/F16-22.jpg)

The parts I needed to CNC for the growbox were fairly simple. I stuck to basic rectangles, with the following dimensions.

- 2 pieces 100 x 31.7 cm
- 2 pieces 29.5 x 31.7 cm
- 1 piece 100 x 33 cm

I decided to use the 120 x 120 x 1.8 cm plywood sheets that the lab had left from the computer-controlled machining week. This material is quite cheap and soft, so I decided to use some stronger wood for the lip around the box, as it would be the component holding most of the growbox's weight.

![Image 23: CNC machine](/images/F16/F16-23.jpg)

![Image 24: Post processing 1](/images/F16/F16-24.jpg)

![Image 25: Sanding](/images/F16/F16-25.jpg)

The main drawback with the pine plywood is that due to it being very soft, it tends to break and splinter very easily while CNCing. Taking into account what I learned from CNC week I used the 6mm downcut tool for the first 3mm, and the upcut tool for the rest of it. In addition I changed the stepover to be only 2mm, with the first and last 3mm or so to be roughly around 1mm. With these settings I was able to keep the cuts relatively clean.

After cutting all the parts I post processed all the pieces by sanding them.

![Image 26: Sides glued and screwed](/images/F16/F16-26.jpg)

![Image 27: Bottom glued and screwed](/images/F16/F16-27.jpg)

The box will need to hold in quite a lot of weight, so I decided not to go for finger joints at all, but instead use wood glue and screws. After gluing I used clamps to hold the pieces together to create a tight hold for the glue.

![Image 28: Side lips glued and screwed](/images/F16/F16-28.jpg)

![Image 29: Glue to be sanded](/images/F16/F16-29.jpg)

![Image 43: Glue sanded](/images/F16/F16-43.jpg)

I found a piece of some harder wood from the leftovers, which was 13.2 mm wide. I had the piece cut into 3 pieces of equal width, 4.4 mm each. I then used the same tactic to attach it to the top sides.

After that I sanded the dried glue from the ends of the box using the palm sander.

![Image 30: Adding height to lips](/images/F16/F16-30.jpg)

![Image 34: Glued lip](/images/F16/F16-34.jpg)

![Image 35: Glued and screwed lip](/images/F16/F16-35.jpg)

The middle piece which I cut down to attach to the ends of the box ended up being about 4 mm thinner than the other pieces. I would like the weight to distribute evenly between all the sides, so I decided to glue some 4 mm plywood on it to make it the same size as the other sides. I then sanded away the portruding plywood pieces and attached the parts to the box.

![Image 39: Hole and seal tests](/images/F16/F16-39.jpg)

![Image 40: Hole drillbit](/images/F16/F16-40.jpg)

![Image 44: Hole](/images/F16/F16-44.jpg)

While I was waiting for some of the glue to dry I was test drilling holes for the bell siphon to go through. On the first photo above you can see the seal I made using silicone, to read more about that see Water circulation page. Important thing to remember while drilling big holes like shown above, is to start drilling on one side, and when the sharp tip punctures the wood, then stop drilling and change to the other side to finish the hole. Otherwise the tool tip would most likely splinter the surface on the wood as the wide part comes through.

I may have been slightly hasty drilling the hole on the growbox. I decided to drill it in the middle of the bottom on one side, without thinking too much of it. A while later I realised it wouldn't work as there would be a metal support beam on the frame right below it.

![Image 31: Milling a plug](/images/F16/F16-31.jpg)

I used the MDX-40 to mill a small piece to plug the hole. The hole that I drilled was 22 mm, and the first plug I made I used the same diameter. This proved to be slightly too small, so I made another one with 23mm diameter, which was a good size.

![Image 32: Glueing hole](/images/F16/F16-32.jpg)

![Image 33: Glued plug](/images/F16/F16-33.jpg)

![Image 36: Sanded plug](/images/F16/F16-36.jpg)

I put in some glue, hammered it down and sanded after the glue had dried.

![Image 46: Varnish](/images/F16/F16-4B.jpg)

![Image 47: Varnish tint](/images/F16/F16-3B.jpg)

![Image 37: Varnishing 1](/images/F16/F16-37.jpg)

![Image 38: Varnishing 2](/images/F16/F16-38.jpg)

I purchased some outdoor water-based furniture varnish with added tint I selected. I did 3 coats in total, sanding after the first coat.

![Image 41: Growbox and frame pieces 1](/images/F16/F16-41.jpg)

![Image 42: Growbox and frame pieces 2](/images/F16/F16-42.jpg)

After this the structrue of the growbox was finished! I was super pleased with the colour it turned out. I brought some parts of the frame to the lab from the metal workshop to test their fit, and was so excited about how good the wood and steel look together.

![Image 45: Plastic](/images/F16/F16-45.jpg)

Next up is to waterproof the growbox. I decided to use some thick plastic, after I found a piece that is used for protecting the floor of the nearby Set Design Workshop. At the moment I'm waiting for a roll to be delivered to the lab.
