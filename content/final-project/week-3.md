+++
title = "Final Project"
Description = "Circulation"
+++

[Update](#seal)

## Water circulation

After researching DIY aquaponics projects I've come to find out there really isn't a standard blueprint for building an aquaponic setup. There are as many designs as there are makers, but the core components that are found in all the aquaponics systems are

- Fishtank
- Growbed
- Water circulation system (out from the fishtank into the growbed, out of the growbed into the fishtank)

In most setups the growbed is situated above the fishtank, and therefore for letting the water out of the growbed, we can use the help of gravity to get the water flowing that direction. However in order to get the water up into the growbed we need to use an electric pump. I have a standard submersive pump from a previous aquarium setup that I will be using in this first prototype, and possibly in the final design as well.

![submersible pump](/week3-1.jpg)

Therefore the fishtank &#8594; growbed water circulation is relatively straightforward, submerge the pump in the fishtank full of water, attach a plastic flexible pipe to the pump and attach the other end on the side of the growbed, and plug it in. This will create a constant flow of water into the growbed.

{{< video src="/water-pump1.webm" type="video/webm" preload="auto" >}}

#### Bell siphon

The growbed &#8594; fishtank water flow is a little bit trickier to figure out. The closed water flow system is a core element in an aquaponics setup, so this felt like a perfect step to start figuring out how to build my final project, as everything else can be designed around it once I have the basic water circulation going.
In most of the aquaponic setups that I saw people were using a bell siphon to release the water from the growbed back into the fishtank. I wasn't previously familiar with bell siphons or how they work, so I set out to learn more about them and how they work. Below is a short video explaining the concept very well.

{{< youtube _vV_z_0lFQ8 >}}

Below is a simple diagram showing the components of a bell siphon. The way it works is the reservoir, or growbed in the case of an aquaponic tank, fills with water until the depth of the water inside the reservoir reaches over the riser pipe and starts flowing down. This cuts off the air flow into the pipe and creates a vacuum that sucks the water down the riser until the water reaches minimum depth. As air gets in the bell the vacuum is broken, and the water flow stops, allowing the cycle to repeat as the water reaches the maximum depth again.

![bell siphon diagram](/bellsiphon-diagram.png)

As shown above the maximum depth of the water is determined by the height of the riser, and the minimum depth by the height of the holes at the bottom of the bell. Below is a diagram showing the whole circulation of water through the aquaponic system. In the aquaponic system a needed addition would also be a media guard, which stops the grow media entering the pipe and therefore the fishtank. However at this point I will only focus on creating the water circulation with water, and will add the grow medium later.

<img src="/circulation-diagram.png" alt="Circulation diagram" width="600">

#### Why bell siphon?

There are many reasons why a bell siphon system is included in many aquaponic and hydroponic systems. Below are a few of them, as I learned from [this](https://gogreenaquaponics.com/blogs/news/what-is-bell-siphon-and-why-we-use-it-in-aquaponics) website.

- Bell siphon allows the water level of the growtank to fluctuate, which ensures the plants' roots are exposed to air regularly, as well as remain hydrated.
- Constant and consistent movement prevents stagnation of the water and improves oxygen.
- The process is automatic, without needing a timer, sensors, other drainage equipment or manual draining.
- Easy to maintain and simple to use.
- Easy to set up and don't require electricity to function.

#### Experiments: Day 1

I went to the nearest hardware store and bought some PVC pipes to create my first prototype. The selection at the shop wasn't the best, and as I knew I would most likely not succeed right away, I wanted to keep my costs as low as possible. I managed to find two different sizes of plastic tubes, that were actually meant for wire installation, but I figured they would do. The diameters of the pipes were 20 mm and 32 mm. This proved to be my first mistake, as the diameter of the outer pipe (bell) would turn out to be too narrow. The website I linked earlier states that the diameter of the bell should be at least twice the diameter of the riser. The website also lists some other components that help create a strong flow that ensures the forming of the vacuum.

![stuff bought from the hardware store](/shop.jpg)

In addition I also bought some pipe connectors, silicone glue, and a standard plant box.

![small water tank](/poc1_fishtank.jpg)

I got a small 170 x 170 x 300 mm aquarium from a classmate to use in my prototype.

After making sure the pump worked fine, as shown on the video I included at the top of this page, I started building the bell siphon. I marked the maximum water level on the inside of the plant box, as the fishtank I had was quite small and I had to make sure the pump would not run dry as that could damage it. I cut a short piece off the 20 mm pipe and marked down the center of the plant box. I made a 20 mm hole on the bottom of the plant box using a power drill and the appropriate size drillbit, and stuck the pipe through it, making sure the height didn't supercede the marked height.

![plant box](/poc1_plantbox.jpg)

![plant box with 20mm hole](/poc1_plantbox_hole.jpg)

After that I used a hot glue gun to seal the hole with the pipe to ensure the connection was airtight, and then I moved on to cutting the bell. I cut the 32 mm pipe down, so that the height was a few cm higher than the riser above the plantbox. I also cut small teeth on the bottom of it to let the water flow through. After this I placed the pipe over the riser and glued it in place, making sure I didn't block the holes at the bottom.

![bell](/poc1_bell.jpg)

![bell + riser 1](/poc1_bell_riser1.jpg)

![bell + riser 2](/poc1_bell_riser2.jpg)

At this point I realised I had forgotten a crucial component, an endcap for the bell. If air could flow in from the top there was no hope for a vacuum to form inside the bell. I used the lasercutter to cut a 32 mm circle out of some scrap acrylic that we had in the fablab, and glued it on top of the bell and made sure it was airtight.

![endcap](/poc1_endcap.jpg)

![bell siphon](/poc1_bellsiphon.jpg)

At this point my first prototype of the bell siphon was finished, and the only thing left to do was try if it worked.

{{< video src="/poc1_test1.webm" type="video/webm" preload="auto" >}}

As can be seen from the video above the bell siphon had two problems.

- The outflow rate is slower than the inflow rate.
- The water stops draining out while the water level is about middle of the bell, instead of at the very bottom.

I suspect that both of these issues stem from the fact that the space between the bell and the riser is too small, as I mentioned above. The water isn't flowing out at a strong enough rate to be able to create a vacuum, and therefore the drain fizzles out before it is supposed to. I also realise now the protoype that I created is not very customisable, as I have secured everything with glue. I should be able to soften the hot glue with a heat gun and remove the bell siphon for the next experiment, however my aim is to create a more modular system, that would be easy to disassemble for fixes and other maintenance. For this I'm thinking of 3D printing some fittings to use with the tubes.

Doing some more research after the experiment I ran into the video below, which is a very interesting project using a bell siphon to empty a rain gutter with a strong water flow and ultimately turn it into electricity. The design of the bell siphon is a bit different than the one I tried, and I'm interested in trying a similar design with the next experiment for a couple reasons. First, it uses same size pipes instead of two different sizes. I already determined that the size difference between my pipes is too small. However I still have plenty of the plastic tubes left, and I'd prefer if I didn't have to buy more material at this stage. Secondly, the bell he is using is clear, and that would help me see what's going on inside, to troubleshoot potential issues in the future. It also looks like it would be easier to adjust the maximum and minimum water levels.

{{< youtube z-GLzQ2bqEk >}}

#### Experiments: Day 2

For the next version I decided to try a similar design to the video I linked above. I used the Assignment 4 to design the part, and talked more about the designing part [here](https://digital-fabrication.fiikuna.fi/assignments/assignment-04/). After 3D printing it I was ready to try it on the prototype. Since the 3D print isn very rigid and doesn't have much tolerance, I was a bit worried about getting it to be airtight. However I was pleasantly surprised how well it fit. The holes were exactly the right size for the pipes, and after I had jammed them in with a little help from a hammer, they were certainly watertight. The glass however on the top part was a snug fit, but still not completely air tight. The glass tapers in at the very top, so I didn't think making the part little bit bigger would help.

![Image: 3D printed part](/images/F4/F4-1.jpg)

![Image: Tape](/images/F4/F4-2.jpg)

I went to the hardware store to find something to use for insulation. The O-rings that they had were too small, so figured I would try some repair tape.

![Image: Insulation](/images/F4/F4-3.jpg)

I wrapped a thin strip on the bottom of the plug and filled the glass with water before fitting it in. There was still some water leaking out, but I thought it would be the best I could get it.

![Image: Watertight test](/images/F4/F4-4.jpg)

![Image: Removing pipe](/images/F4/F4-5.jpg)

Next I needed to remove the v1 bell siphon. I had used a standard hot glue gun to attach the pipe on the growbed, so I was able to remove it by using a heat gun. Since this is only a testing prototype, it doesn't really matter what type of glue I use, but for the actual thing I will have to make sure it doesn't leak any chemicals to the water that would be harmful for the fish.

![Image: Hole](/images/F4/F4-6.jpg)

![Image: Cut pipe](/images/F4/F4-7.jpg)

The smaller pipe was a bit too short for the growbed so I had to cut it down.

{{< video src="/images/F4/F4-V1.webm" type="video/webm" preload="auto" >}}

After attaching the siphon it was time to test it. As you can see from the video, it wasn't a success. The outflow still wasn't strong enough to create a siphon.

![Image: Broken glass](/images/F4/F4-8.jpg)

As I was doing some troubleshooting the glass ended up breaking, which made me realise I had to abandon this concept and start over, again.

#### Keep it simple, stupid

I had found some videos were people were just using cut plastic bottles as the bell. At that point I was already in the process of making the 3D printed part, and I wanted to see if it would work. But I was definitely trying to overengineer it. After the v2 failed I figured I just needed to get it to work.

![Image: Plastic bottle](/images/F4/F4-9.jpg)

{{< video src="/images/F4/F4-V2.webm" type="video/webm" preload="auto" >}}

I cut the bottom out of a small plastic bottle and placed it on top of the riser pipe, and filled out the growbed. The water started flowing with a good force and the bell siphon was working perfectly! It definitely made me confident knowing that as long as the bell is wide enough, I don't need to do anything too complicated.

One thing that got me thinking was the water level changing in the 'fish tank'. I would either have to make the fish tank bigger, to account for the extra water, or make another sump tank to hold the extra water while it isn't in the grow bed. I'm thinking of going this route as I'm not sure if the constantly changing water levels would stress out the fish.

![Image: Water circulation diagram](/images/F4/F4-14.png)

This is an updated diagram that shows the sump tank as well. The video I linked above with the bell siphon in the rain gutter the guy was using the strong water flow to generate electricity. I liked the idea and I was thinking if I could include that in my aquaponics setup as well, to make the project even more sustainable. This might be too ambitious to me, but it is something I'm considering. I also don't know how much electricity I would be able to generate, and whether it would be enough to even run the lights.

Anyway for this setup to work the sump tank outflow needs to be the same as the fish tank outflow, so that was the last think I set out to do.

![Image: Weighing tank](/images/F4/F4-10.jpg)

![Image: Weighing 3l of water](/images/F4/F4-11.jpg)

I weighed the aquarium and then filled it up to 3 litres (3000g) of water.

![Image: Marking 3l](/images/F4/F4-12.jpg)

I then marked the line where 3l of water would be.

![Image: Putting in the pump](/images/F4/F4-13.jpg)

I emptied the tank, and connected the pipe to start filling it. I used a stopwatch on my phone to see how long it would take to fill 3l of water. The time ended up being 27 seconds, which I then converted to cubic centimeters per second, which is about 111 cm3/s. Next step is to figure out how big the pipe out of the sump tank needs to be to maintain that water flow rate.

![Image: Animated diagram](/images/F4/F4-15.png)

The above diagram shows the water flow through my planned system.

#### Hydropower Generator research

I started doing more research on hydropower generators. Youtuber Quint Builds, whose video I linked earlier in this page regarding his bell siphon design, does a great job at explaining his project throughout. His first video in the Rain Gutter Power-series helped me a lot when I started doing more research on the generator.

{{< youtube S6oNxckjEiE >}}

Using his calculations I tried to estimate the potential power that could be available to me if I tried to harvest it. I calculated the flow from the video I shot of the bell siphon working, and other values are estimates based on my current plans.

![Image: Wattage calculations](/images/F4/F4-16.jpg)

I quickly realised the potential power harvested would be tiny, and not worth attempting. I could possibly improve the flow and other values a bit and increase the potential wattage through iterations, but I still don't think it would be worth it, so I am leaning on abandoning this idea altogether.

# Seal

Before starting to work on the final version of the bell siphon, I needed to figure out how I would seal the hole through which the bell siphon pipe would go through. One option would be to glue it in with silicone, and therefore to seal all the possible holes that ways. The advantage of this would be that it would be very easy to make sure that the hole is watertight, and there is no water dripping out. The disadvantage would be that the siphon would be permanently fixed to the growbed permanently, which I didn't want. Instead I wanted to try and make a custom seal out of silicone, that would stay between the hole and the pipe.

![Image 1: seal 3D](/images/F3/F3-1.jpg)

![Image 2: Seal wax mold](/images/F3/F3-2.jpg)

I followed the process I learned during the molding and casting week, to make a custom mold for the seal, first in Fusion 360, and then milled it out of the wax. The piece of wax I used was slightly too small, and the design was cut on one side. This was fine however, I just used normal tape to create a wall to keep all the silicone in.

![Image 3: seal silicone cast](/images/F3/F3-3.jpg)

![Image 4: ](/images/F3/F3-4.jpg)

I used Smooth-on Sorta-Clear 37 translucent silicone, because it is certified food safe, and therefore should be safe to use in contact with the aquarium water.

![Image 5: ](/images/F3/F3-5.jpg)

![Image 6: ](/images/F3/F3-6.jpg)

I tested the seal first on the plastic growbox I used for my previous bell siphon tests. The hole that I made was too big for the seal, and it didn't hold the water. Having learned from this, I made some test holes in wood, and tried the seal with those. I ended up tearing the seal trying to get it to fit in the wood in smaller holes, so I needed to cast a new one.

![Image 13: ](/images/F3/F3-13.jpg)

![Image 14: ](/images/F3/F3-14.jpg)

I made the walls of the seal slightly thinner, which proved to be a problem while trying to mill the wax again. Instead of trying again I 3D printed the mold using one of the 3D printers at the lab, and casted the seal again.

I then used a bucket I had purchased previously to try the seal again. I made the hole to be about 24mm in diameter, which was very tight. I carefully stuffed the pipe with the seal attached through it, and filled the bucket with water which to my delight proved to be watertight, and no water was leaking through!

{{< video src="/images/F3/F3-V1.webm" type="video/webm" preload="auto" >}}

![Image 7: ](/images/F3/F3-7.jpg)

Next I set out to work on the bell part of the bell siphon. I purchased 1m of 50mm diameter PVC pipe, with one end cap, as well as a 75mm joiner with an endcap for that as well.

![Image 8: ](/images/F3/F3-8.jpg)

![Image 9: ](/images/F3/F3-9.jpg)

My plan was to use the 75mm pipe as a media guard. I used the dremel to cut thin holes to let the water through, but not any of the grow media.

![Image 10: ](/images/F3/F3-10.jpg)

![Image 11: ](/images/F3/F3-11.jpg)

![Image 12: ](/images/F3/F3-12.jpg)

Next I cut a 50mm hole on the top of the media guard end cap, so that I could slot the 50mm pipe through it. I used the dremel to cut a rough shape first, and then a round file to make the cut slightly smoother.

![Image 13: ](/images/F16/F16-1B.jpg)

![Image 14: ](/images/F16/F16-2B.jpg)

Next I cut down the 50mm pipe to the proper length and cut two large holes on the bottom. Then I tried fitting them together to see what the final bell would look like with the media guard.
