+++
title = "Resources"
description = "Resources I found useful throughout the course"
+++

I will collect links here for resources I found useful during the course.

#### Web Development

[Hugo's Directory Structure Explained](https://www.jakewiesler.com/blog/hugo-directory-structure)
[A Complete Guide to Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

#### Final Project

[Assistive Aquaponics Fish Tank - Teddy Warner - Fab Academy 2021](https://fabacademy.org/2021/labs/charlotte/students/theodore-warner/Final%20Project/final-project/)

[Aquapioneers - Guillaume Teyssie - Fab Academy 2016](http://archive.fabacademy.org/archives/2016/greenfablab/students/365/project03.html)

#### Other

Aquaponic icon on the home page by [Nithinan Tatah from NounProject.com](https://nounproject.com)