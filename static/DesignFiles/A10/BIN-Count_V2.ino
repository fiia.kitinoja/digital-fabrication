/* 
 
  Binary counter

  Push of the button increases the count by one, current count shown in binary with the LEDs.
  Max count is 16, next increase resets the counter.

  Fiia Kitinoja
  6 March 2022

*/

int pin0=6; // LSB
int pin1=7;
int pin2=8;
int pin3=9; // MSB
int btn=10;

int count=0;
int buttonState;
int lastButtonState=LOW;

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); // open the serial port at 9600 bps:
  pinMode(pin0, OUTPUT);
  pinMode(pin1, OUTPUT);
  pinMode(pin2, OUTPUT);
  pinMode(pin3, OUTPUT);
  pinMode(btn, INPUT_PULLUP);
  resetLEDs();
}

void loop() {
//  count++;
//  updateLEDs();
//  delay(1000);
  // put your main code here, to run repeatedly:
  int reading = digitalRead(btn);
  if(reading != lastButtonState) {
    // Reset debounce timer
    lastDebounceTime = millis();
  }
  if ((millis() - lastDebounceTime) > debounceDelay) {
    if(reading != buttonState) {
      buttonState = reading;
      if(buttonState == HIGH) {
        count=count+1;
        updateLEDs();
      }
    }
  }
  lastButtonState = reading;
 }

void updateLEDs() {
  if (count >= 16) {
    resetLEDs();
    count=0;
  } else {
    digitalWrite(pin0, bitRead(count, 0));
    digitalWrite(pin1, bitRead(count, 1));
    digitalWrite(pin2, bitRead(count, 2));
    digitalWrite(pin3, bitRead(count, 3));
  }
}

void resetLEDs() {
  digitalWrite(pin0, LOW);
  digitalWrite(pin1, LOW);
  digitalWrite(pin2, LOW);
  digitalWrite(pin3, LOW);
}
